import { Box, Button, FormControl, Grid, InputLabel, Link, TextField, Typography } from "@mui/material";
import { Children } from "react";
import styled from "styled-components";

const StyledFormControl = (props) => (
  <FormControl sx={{
    rowGap: {
      mobile: 1,
      tablet: 3,
      laptop: 4
    },
    paddingInline: "5%",
    // backgroundColor: "#43a047",
    width: {
      mobile: "60%",
      tablet: "50%",
      laptop: "30%"
    }
  }}
  >{props.children}</FormControl>
)


const PrimarySubmitButton = (props) => (
  < Button
    type="submit"
    variant="contained"
    sx={{
      // color: "red",
      fontSize: 15
    }}

  > {props.children}</Button >
)

const StyledLink = (props) => (
  <Link
    sx={{
      color: "white",
      marginInline: 2
    }}
  >{props.children}</Link>
)

const Page = ({ children, ...props }) => (
  <Box
    sx={{
      paddingTop: "70px",
      height: "100%",
      margin: {
        // mobile: "unset",
        tablet: "0 1vw"
      }
      // margin: "0 1vw"
    }}
  > {children}</Box >
)

const GridRowSection = (props) => {
  const { sx, children, ...rest } = props;
  { console.log("children: ", Children.count(children)) }
  const arrayChildren = Children.toArray(children);
  const chProps = Children.map(arrayChildren, (child, index) => {
    return (
      <Grid item key={index} mobile={(12 / Children.count(children)) - 1}>
        {child}
      </Grid>
    )
  })

  return (
    <Grid container
      item
      // rowSpacing={{ mobile: 1, tablet: "1vh" }}
      columnSpacing={{ mobile: 1, tablet: "1vh" }}
      // mobile={12}
      // wrap
      sx={{
        paddingBlock: {
          tablet: 1
        },
        // marginBlock: {
        //   mobile: 1,
        //   tablet: "1vh"
        // },
        // gap: {
        //   mobile: 0,
        //   // tablet: 2
        // },

        justifyContent: {
          mobile: Children.count(children) === 1 ? "start" : "space-around",
          // tablet: "start"
        },
        ...sx
        // gap: { mobile: 5 }
        // flex: 1

      }}
      {...rest}
    >
      {children}
      {/* {chProps} */}
    </Grid >
  )

}


// const StyledLablel = (props) => (
//   <InputLabel {...props} >{props.children}</InputLabel>
// )

const ErrorDisplay = (props) => (
  <Typography variant='body2' align="center"
    sx={{
      color: "red"
    }}
    {...props}
  >{props.children}</Typography>
)

const StyledTextField = (props) => {
  const { sx, children, InputProps, helperText, ...rest } = props
  return (
    <Grid item
      mobile={props.fieldwidth}
      // tablet={props.fieldwidth}
      sx={{
        // paddingRight: "10px",
        marginTop: {
          mobile: "2vw",
          tablet: 0
        }
      }}>

      {/* {props.type === "date" &&
        <InputLabel shrink htmlFor={props.id}

          sx={{
            WebkitTextFillColor: "primary"
          }}
          color="primary">
          {props.fieldlabel}

        </InputLabel>} */}


      <FormControl
        sx={{
          width: "100%"
        }}
      >
        <TextField
          label={props.fieldlabel}
          // label={props.type !== "date" && props.fieldlabel}
          fullWidth
          size="small"
          helperText={helperText}
          sx={{

            "& .MuiInputBase-input.Mui-disabled": {
              WebkitTextFillColor: "black",
              // padding: "10px",
              // minWidth: "fit-content",
              width: "100%",
              // border: 0
              // textAlign: "center",

            },
            // minWidth: "fit-content",
            minWidth: "inherit",

            // border: "none",
            marginBottom: "8px",
            // boxShadow: props.disabled ? `hsl(280deg 70% 35% / 60%) 0px 0px 5px 0px  inset` : "none",
            borderRadius: "5px",
            ...sx
          }}
          InputProps={{
            border: "none",
            ...InputProps,
            // disableUnderline: true
          }}
          {...rest}
        >
          {children}
        </TextField>
      </FormControl>

    </Grid >
  )
}



export { StyledFormControl, PrimarySubmitButton, Page, StyledLink, GridRowSection, StyledTextField, ErrorDisplay }