export class BillAccount {
    constructor(accountName = "", accountType = "", accountAdminList = [], accountUserList = []) {
        this.accountName = accountName;
        this.accountType = accountType;
        this.accountAdminList = accountAdminList;
        this.accountUserList = accountUserList;
    }

}