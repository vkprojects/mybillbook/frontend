export class Transaction {
    constructor(transactionFrom = "", transactionTo = "", transDirection = "", transactionType = "", transactionDate = new Date(), transactionBank = "", transactionAmount = "") {
        this.transactionFrom = transactionFrom;
        this.transactionTo = transactionTo;
        this.transDirection = transDirection;
        this.transactionType = transactionType;
        this.transactionDate = transactionDate;
        this.transactionAmount = transactionAmount;
        this.transactionBank = transactionBank
    }
}