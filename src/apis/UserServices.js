import axios from "axios"
import { BASE_URL } from "../constants"

export const userLogin = async (reqBody) => {
    // const response = await axios.post("http://localhost:8080/signin", reqBody)
    const response = await axios.post(`${BASE_URL}/signin`, reqBody)
    return response
}

export const getAllTransactions = async (accountId) => {
    const reqHeader = {
        "user": "vedanthi@gmail.com",
        "Content-Type": "application/json"
    }

    return await axios.get(`${BASE_URL}/transaction/getAllTransactions/6440b7445a04fd4bfb965673`, { headers: reqHeader })
}