import React, { useEffect } from 'react'

import { Box, Button, Link, TextField, Typography } from '@mui/material'
import '../styles/login/login.css'
import { StyledFormControl, PrimarySubmitButton, Page } from "../styles/login/styles.js"
// import { GoogleLogin } from '@react-oauth/google'
import { useGoogleLogin } from '@react-oauth/google';

import useFormSubmit from '../hooks/useSubmit'
import { useDispatch, useSelector } from 'react-redux';
import { removeError, setUserAttribute, setUserError, resetAttribute, resetError } from '../redux/dataSlice'
import useForm from '../hooks/useForm';
import axios from 'axios';
import useNewForm from '../hooks/useForm';
import { useNavigate } from 'react-router-dom';
import { useFormFieldsHandle, useFormValidateAndSubmit } from '../hooks/useFormValidateSubmit.js';
import { signin_signupValidation } from "../formValidations/signin_signupValidation.js"
import { BASE_URL } from '../constants.js';



const SignUpPage = () => {


    const navigate = useNavigate()
    const dispatch = useDispatch()
    const user = useSelector(state => state.user.attributes)
    const error = useSelector(state => state.user.error)

    const { handleTextChange, handleListChange, handleSelectChange, handleSetAllFields, resetAllAttributes } = useFormFieldsHandle({
        setFieldAttribute: setUserAttribute,
        resetFieldAttributes: resetAttribute
    })

    const { handleValidateAndSubmit, handleValidateField, isLoading } = useFormValidateAndSubmit({
        url: `${BASE_URL}/v1/signup`,
        method: "post",
        formValidation: signin_signupValidation,
        errorState: error,
        setErrorState: setUserError
    })


    const login = useGoogleLogin({
        onSuccess: async tokenResponse => {
            await axios.get("https://www.googleapis.com/oauth2/v3/userinfo", {
                headers: {
                    "Authorization": `Bearer ${tokenResponse.access_token}`
                }
            }).then((res) => {
                if (res.status === 200)
                    console.log(res)
            }).catch(err => {
                console.log(err)
            })
        }
    });




    // const { handleChange, handleValidate, handleSelectChange, submitValidation, setSubmitValidation } = useNewForm(user, error, setUserAttribute, setUserError, removeError)
    // const { isPending, response, setIsPending, handleSubmit, handleSubmitOnClick, errorResponse } = useFormSubmit("http://localhost:8080/v1/signup", user)

    // console.log(user, "::", error)
    console.log("SignUp page")

    const handleFormSubmit = async (event) => {
        event.preventDefault()
        console.log("request: ", user)
        const response = await handleValidateAndSubmit(user, true)
        console.log("SignUp Response: ", response)
        if (response.error === null) {
            navigate("/login")
        }
        else {
            console.log(response.status === 400)
            if (response.status === 400) {
                console.log("Setting Error: ", response.error?.details)
                dispatch(setUserError({ ...response.error?.details }))
            }
        }
        // handleValidate()
    }

    // useEffect(() => {
    //     if (submitValidation) {
    //         console.log("submit validate error : ", error, "::", Object.keys(error).length)
    //         if (Object.keys(error).length === 0 && Object.keys(user).length !== 0) {
    //             handleSubmit(true)
    //             setSubmitValidation(false)
    //         }
    //     }
    // }, [submitValidation])

    // useEffect(() => {
    //     console.log("Response: ", response)
    //     if (response !== null) {
    //         navigate("/login")
    //         dispatch(resetAttribute())
    //     }
    //     console.log(!!errorResponse)
    // }, [response, errorResponse])

    useEffect(() => {
        console.log("useeffect ")
        console.log("Data: ", user)
        console.log("Error: ", error)

    }, [])


    return (
        <Page>
            <form onSubmit={handleFormSubmit}>
                <StyledFormControl>
                    <Typography sx={{
                        textAlign: "center",
                    }}
                        color="primary"
                        variant='h4'>Register</Typography>
                    <TextField
                        autoFocus
                        id="standard-email"
                        label="Email"
                        type="email"
                        variant="standard"
                        name='email'
                        onChange={handleTextChange}
                    />
                    <TextField
                        id="standard-password"
                        label="Password"
                        type="password"
                        variant="standard"
                        name='password'
                        onChange={handleTextChange}
                    />
                    <PrimarySubmitButton type="submit" >Submit </PrimarySubmitButton>
                    <Button
                        variant='outlined'
                        onClick={() => { }}>
                        Sign Up with Google
                    </Button>
                    <Box
                        component="div"
                        sx={{ display: 'flex', justifyContent: 'center', gap: 3 }}>
                        <Link sx={{
                            fontSize: 13
                        }}
                            variant='string' href='/'>Login</Link>

                    </Box>

                </StyledFormControl>
            </form>
        </Page>
    )
}


export default SignUpPage