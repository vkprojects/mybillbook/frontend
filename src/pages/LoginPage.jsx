import React, { useContext, useEffect, useLayoutEffect, useRef } from 'react'

import { Box, Button, ButtonGroup, Link, TextField, Typography } from '@mui/material'
import '../styles/login/login.css'
import { StyledFormControl, PrimarySubmitButton, Page, HorizontalButtonGroup, Btn, ErrorDisplay } from "../styles/login/styles.js"
// import { GoogleLogin } from '@react-oauth/google'
import { useGoogleLogin } from '@react-oauth/google';

import useFormSubmit from '../hooks/useSubmit'
import { useDispatch, useSelector } from 'react-redux';
import { removeError, setUserAttribute, setUserError, resetAttribute, resetError } from '../redux/dataSlice'
import useForm from '../hooks/useForm';
import { useNavigate } from 'react-router-dom';
import useNewForm from '../hooks/useForm';
import axios from 'axios';
import useAuth from '../hooks/useAuth';
import useAuthData from '../hooks/useAuthData';
import { BASE_URL } from '../constants.js';




const NewLoginPage = () => {

    const { setAuth } = useAuth()

    const user = useSelector(state => state.user.attributes)
    const error = useSelector(state => state.user.error)
    const { setAuthAuthData } = useAuthData()
    // console.log()


    const { handleChange, handleValidate, handleSelectChange, submitValidation, setSubmitValidation } = useNewForm(user, error, setUserAttribute, () => { }, setUserError, removeError)
    const { isPending, response, setIsPending, handleSubmit, handleSubmitOnClick, errorResponse } = useFormSubmit(`${BASE_URL}/v1/signin`, user)

    const dispatch = useDispatch()

    useEffect(() => {
        console.log("login loaded")
    }, [])


    // const setLocalStorage = () => {
    //     localStorage.setItem("localData", JSON.stringify(storageData))
    // }


    // const loginWithGoogle = useGoogleLogin({
    //     onSuccess: async tokenResponse => {
    //         await axios.get("https://www.googleapis.com/oauth2/v3/userinfo", {
    //             headers: {
    //                 "Authorization": `Bearer ${tokenResponse.access_token}`
    //             }
    //         }).then((res) => {
    //             if (res.status === 200)
    //                 console.log(res)
    //         }).catch(err => {
    //             console.log(err)
    //         })
    //     }
    // });

    // const loginWithPassword = () => {

    // }


    const navigate = useNavigate()


    // console.log(user, "::", error)
    // console.log("Resposne login :", response)

    // useEffect(() => {
    //   first+
    // }, [])




    useEffect(() => {
        if (submitValidation) {
            console.log("submit validate error : ", error, "::", Object.keys(error).length)
            if (Object.keys(error).length === 0 && Object.keys(user).length !== 0) {
                handleSubmit(true)
                setSubmitValidation(false)
            }
        }
    }, [submitValidation])


    useEffect(() => {
        console.log("Response: ", response)
        if (response !== null) {
            setAuth(true)
            console.log(response?.details?.email)
            console.log(response?.details?.tokenDetails?.token)
            console.log(response.details?.userData?.email)
            setAuthAuthData(response?.details?.tokenDetails?.token, response.details?.userData?.email)
            navigate("/")
            dispatch(resetAttribute())
        }
        console.log(!!errorResponse)
    }, [response, errorResponse])


    // useEffect(() => {
    //     // console.log("Login page Resposne: ", response)

    //     if (response?.status === 200) {
    //         console.log("Success")
    //         localStorage.setItem("username", user.email)
    //         navigate("/main")
    //     } else {
    //         localStorage.removeItem("username")
    //         console.log("failure")
    //     }
    // }, [response])



    // console.log("Data: ", user)
    // console.log("Error: ", error)

    const handleFormSubmit = (event) => {
        event.preventDefault()
        handleValidate()
    }

    return (
        <Page>
            <form onSubmit={handleFormSubmit}>
                <StyledFormControl>
                    <Typography sx={{
                        textAlign: "center",
                    }}
                        color="primary"
                        variant='h4'>Login</Typography>
                    <TextField
                        autoFocus
                        id="standard-email"
                        label="Email"
                        type="email"
                        variant="standard"
                        name='email'
                        value={user.email}
                        onChange={handleChange}
                    />
                    <TextField
                        id="standard-password"
                        label="Password"
                        type="password"
                        variant="standard"
                        name='password'
                        value={user.password}
                        onChange={handleChange}
                    />
                    <PrimarySubmitButton type="submit" >Submit</PrimarySubmitButton>
                    {/* <Button
                        variant='outlined'
                        onClick={login}>
                        Sign in with Google
                    </Button> */}
                    <Box
                        component="div"
                        sx={{ display: 'flex', justifyContent: 'center', gap: 3 }}>
                        <Link sx={{
                            fontSize: 13
                        }}
                            variant='string' href='/signup'>SignUp</Link>
                        <Link sx={{
                            fontSize: 13
                        }}
                            href='#'>Forgot Password?</Link>
                    </Box>

                    {!!errorResponse && <ErrorDisplay >Invalid Credentials</ErrorDisplay>}

                </StyledFormControl>
                {/* <li */}
            </form>

        </Page>
    )
}


export default NewLoginPage