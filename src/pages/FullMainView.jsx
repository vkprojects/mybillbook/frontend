import { Grid } from '@mui/material'
import { Box } from '@mui/system'
import React, { useState, useEffect, useContext } from 'react'
import { useNavigate } from 'react-router-dom'
// import ContentView from '../components/ContentView'
import ListSideView from '../components/ListSideView'
// import { ManageAccounts } from '../components/ManageAccounts'
import AccountTransactionView from '../components/AccountTransactionView'
// import Profile from '../components/Profile'
import SettingsView from '../components/SettingsView'
import { Page } from '../styles/login/styles'
import { ContactEmergency, Person, Settings, SupervisorAccount } from '@mui/icons-material'
import Profile from '../components/Profile'
import { pageItems } from "../data/PageItems"
import MainAppBar from '../components/MainAppBar'
import AppbarContext from '../context/AppBarProviderContext'


const FullMainView = () => {

    const { currentIndex } = useContext(AppbarContext)

    return (
        <Page>
            {/* <MainAppBar currentIndex={currentIndex} setCurrentIndex={setCurrentIndex} /> */}
            {/* <Box sx={{ display: "flex" }}> */}
            <Grid container sx={{ height: "100%", flexWrap: "nowrap" }}>
                {/* <Grid item >
                    <ListSideView currentIndex={currentIndex} setCurrentIndex={setCurrentIndex} />
                </Grid> */}
                <Grid item sx={{
                    flex: 1,
                    padding: {
                        mobile: "0 2%",
                        // tablet: "16px"
                    }
                }}>
                    {pageItems[currentIndex].page}
                </Grid>
            </Grid>
        </Page>
    )
}

export default FullMainView