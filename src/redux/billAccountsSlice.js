import { createSlice } from "@reduxjs/toolkit";
import { BillAccount } from "../models/BillAccount"
import { useCookies } from "react-cookie";

let billAccountAttribute = JSON.stringify(new BillAccount())
const jsonAttribute = JSON.parse(billAccountAttribute)
const billAccount = {
    "attributes": jsonAttribute,
    "error": {}
}

const billAccountSlice = createSlice({
    name: "billAccount",
    initialState: billAccount,
    reducers: {

        setBillAccountAttributes: (state, action) => {
            state.attributes = { ...state.attributes, ...action.payload }
        },
        setBillAccountAttribute: (state, action) => {
            console.log("State: ", state.attributes)
            if (action.payload.hasOwnProperty("accountAdminList")) {
                console.log("Setting: ", action.payload.accountAdminList)
                state.attributes.accountAdminList.push(action.payload.accountAdminList)
            }
            else if (action.payload.hasOwnProperty("accountUserList")) {
                console.log("Setting: ", action.payload.accountUserList)
                state.attributes.accountUserList.push(action.payload.accountUserList)
            }
            else {
                state.attributes = { ...state.attributes, ...action.payload }

            }

        },
        removeBillAccountAttribute: (state, action) => {

            if (action.payload.hasOwnProperty("accountAdminList")) {
                state.attributes.accountAdminList = state.attributes.accountAdminList.filter((email, index) => action.payload.accountAdminList !== index)
            }
            else if (action.payload.hasOwnProperty("accountUserList")) {
                state.attributes.accountUserList = state.attributes.accountUserList.filter((email, index) => action.payload.accountUserList !== index)
            }
            else {
                delete state.attributes[action.payload]
            }
        },
        setBillAccountError: (state, action) => {
            console.log("SETTING ERROR", action.payload)
            // state.error = { ...state.error, ...action.payload }
            state.error = { ...action.payload }
        },
        removeError: (state, action) => {
            delete state.error[action.payload]
        },
        resetError: (state) => {
            state.error = {}
        },
        resetAttribute: () => billAccount
        // resetAttribute: (state) => {
        //     state.attributes = jsonAttribute;
        //     state.error = {}
        // }
    },
})

export const { setBillAccountAttribute, setBillAccountAttributes, removeBillAccountAttribute, setBillAccountError, removeError, resetError, resetAttribute } = billAccountSlice.actions

export default billAccountSlice.reducer
