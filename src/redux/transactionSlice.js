import { createSlice } from "@reduxjs/toolkit";
import { Transaction } from '../models/Transaction';

// const user = localStorage.getItem()
const transactionAttribute = JSON.stringify(new Transaction("Vedanthi N"))
const jsonAttributeObj = JSON.parse(transactionAttribute)
const transaction = {
    "attributes": jsonAttributeObj,
    "error": {}
}


const transactionsSlice = createSlice({
    name: "transactions",
    initialState: transaction,
    reducers: {
        setTransactionAttribute: (state, action) => {
            // console.log(action.payload)
            state.attributes = { ...state.attributes, ...action.payload }
        },
        setTransactionError: (state, action) => {
            console.log(action.payload)
            state.error = { ...state.error, ...action.payload }
        },
        removeError: (state, action) => {
            delete state.error[action.payload]
        },
        resetError: (state) => {
            state.error = {}
        },
        resetAttribute: () => transaction
        // resetAttribute: (state) => {
        //     state.attributes = transaction.attributes,
        //         state.error = {}
        // }
    },
})

export const { setTransactionAttribute, setTransactionError, removeError, resetError, resetAttribute } = transactionsSlice.actions

export default transactionsSlice.reducer
