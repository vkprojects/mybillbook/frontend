import { configureStore } from '@reduxjs/toolkit'
import userReducer from "./dataSlice"
import billAccountReducer from "./billAccountsSlice"
import transactionReducer from './transactionSlice'


// { console.log(userReducer) }

export default configureStore({
    reducer: {
        user: userReducer,
        billAccount: billAccountReducer,
        transaction: transactionReducer
    },
})