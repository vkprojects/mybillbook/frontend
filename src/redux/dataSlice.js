import { createSlice } from "@reduxjs/toolkit";
import { User } from "../models/User";

const userAttribute = JSON.stringify(new User())

const user = {
    "attributes": JSON.parse(userAttribute),
    "error": {}
    // localStorage.getItem('billBookUser'),
}


const dataSlice = createSlice({
    name: "user",
    initialState: user,
    reducers: {
        // setUserData: (state, action) => {
        //     console.log(action.payload)
        //     return action.payload
        // },

        setUserAttribute: (state, action) => {
            // console.log(action.payload)
            state.attributes = { ...state.attributes, ...action.payload }
        },
        setUserError: (state, action) => {
            // console.log(action.payload)
            state.error = { ...state.error, ...action.payload }
        },
        resetAttribute: (state) => {
            state.attributes = JSON.parse(userAttribute)
        },
        resetError: (state) => {
            state.error = {}
        },
        removeError: (state, action) => {
            delete state.error[action.payload]
        }
    },
})

export const { removeError, setUserAttribute, setUserError, resetAttribute, resetError } = dataSlice.actions

export default dataSlice.reducer
