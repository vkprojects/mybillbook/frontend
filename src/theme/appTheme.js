import { colors } from '@mui/material';
import { red, yellow } from '@mui/material/colors';
import { createTheme } from '@mui/material/styles';

let commonTheme = createTheme({
    breakpoints: {
        values: {
            mobile: 0,
            tablet: 640,
            laptop: 1280,
            // desktop: 1280,
        },
    },
    palette: {
        primary: {
            main: '#671B99',
        },
        secondary: {
            main: '#562DBF',
        },
        success: {
            main: "#45A822"
        },
        error: {
            main: "#F64D4D"
        }
    },
    typography: {
        fontFamily: "'Merriweather', serif;"
    }

})

const darkTheme = createTheme(commonTheme, {
    palette: {
        background: {
            main: "#61395D"
        }
    }
});


const lightTheme = createTheme(commonTheme, {
    palette: {
        background: {
            main: "#671B99"
        }

    }
});



export { darkTheme, lightTheme } 