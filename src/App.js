import React, { Suspense, useEffect } from 'react';
import './App.css';
// import styled from '@emotion/styled'
import { CssBaseline, ThemeProvider } from '@mui/material';
import { darkTheme, lightTheme } from './theme/appTheme';

// import { ToggleButton } from "@mui/material";
// import MainAppBar from './components/MainAppBar';
import { Route, Routes } from 'react-router-dom';
// import HomePage from './pages/HomePage';
import LoginPage from './pages/LoginPage';
import SignUpPage from './pages/SignUpPage';
// import FullMainView from './pages/FullMainView';

// import RequiredAuth from './components/RequiredAuth';
// import useAuth from './hooks/useAuth';
// import CreateAccountForm from './pages/CreateAccountForm';
// import RequiredAppBar from './components/RequiredAppBar';
import Loading from './components/Loading';
// import CreateAccountBtn from './components/CreateAccountBtn';

// import { StaticRouter } from "react-router";

// const HomePage = React.lazy(() => import('./pages/HomePage'));
const FullMainView = React.lazy(() => import('./pages/FullMainView'))
// const CreateAccountForm = React.lazy(() => import('./pages/CreateAccountForm'))
const RequiredAuth = React.lazy(() => import('./components/RequiredAuth'))
const RequiredAppBar = React.lazy(() => import('./components/RequiredAppBar'))
// const LoginPage = React.lazy(() => import('./pages/LoginPage'))
// const SignUpPage = React.lazy(() => import('./pages/SignUpPage'))


function App() {
  // const [isLogged, setIsLoggedState] = useState(false)

  useEffect(() => {
    console.log("App loaded")
  }, [])


  return (
    <ThemeProvider theme={darkTheme}>

      <CssBaseline />
      <Routes>

        {/* <Route exact path="test" element={<TestPage />} /> */}
        <Route exact path="/signup" element={<SignUpPage />} />

        <Route index exact path="/login" element={<LoginPage />} />

        {/* {console.log("loading RequiredAuth")} */}
        <Route exact path="/">
          <Route element={<RequiredAuth />}>
            {console.log("loading RequiredAuth")}
            <Route element={<RequiredAppBar />}>
              <Route index element={
                <Suspense fallback={<Loading />}> <FullMainView /></Suspense>

                // <FullMainView />
              }
              />
              {/* <Route path='account/'>
                {console.log("loading account")}
                <Route index path='create' element={
                  <Suspense fallback={<Loading />}> <CreateAccountForm /></Suspense>
                } />
              </Route> */}
            </Route>
            {console.log("loading others")}
            <Route path='*' />
          </Route>


          {/* <Route exact path="login" element={!isLogged ? <LoginPage setLoggedInStatus={setIsLogged} /> : <Navigate to="/main" replace />} /> */}
          {/* <Route exact path="login" element={<LoginPage setLoggedInStatus={setIsLoggedState} />} />
          <Route exact path='' element={isLogged ? <FullMainView /> : <Navigate to="/login" />} /> */}
          {/* <Route index element={isLogged && <Navigate to="/" replace />} /> */}
        </Route>
      </Routes>
    </ThemeProvider>
  );
}


// const ThemeToggle = (props) => (
//   <Switch
//     sx={{
//       position: "sticky",
//       zIndex: 10000
//     }}
//   >
//     {props.children}
//   </Switch>
// )

export default App;
