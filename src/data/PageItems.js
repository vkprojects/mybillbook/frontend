import React from "react";
import { ContactEmergency, Person, Settings, SupervisorAccount } from "@mui/icons-material";
import ReactSuspence from "../components/ReactSuspence";
// import AccountTransactionView from "../components/AccountTransactionView";
// import Profile from "../components/Profile";
// import SettingsView from '../components/SettingsView'
// import TransactionList from "../components/TransactionList";
// import Accounts from "../components/Accounts";

const Profile = React.lazy(() => import("../components/Profile"));
const SettingsView = React.lazy(() => import("../components/SettingsView"));
const AccountHomePageView = React.lazy(() => import("../components/AccountHomePageView"));
const Accounts = React.lazy(() => import("../components/Accounts"));



export const pageItems = [
    { "label": "Bill Accounts", "icon": <SupervisorAccount />, "page": <AccountHomePageView /> },

    // { "label": "Bill Accounts", "icon": <SupervisorAccount />, "page": <ManageAccounts /> },
    { "label": "Profile", "icon": <Person />, "page": <ReactSuspence><Profile /></ReactSuspence> },
    { "label": "Settings", "icon": <Settings />, "page": <ReactSuspence><SettingsView /></ReactSuspence> },
    { "label": "Contacts", "icon": <ContactEmergency />, "page": <ReactSuspence><div>Not set Contact</div></ReactSuspence> },
    { "label": "Accounts", "icon": <ContactEmergency />, "page": <ReactSuspence><Accounts /></ReactSuspence> }

]