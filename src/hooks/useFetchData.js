import axios from "axios"
import { useEffect, useState } from "react"
import useAuthData from "./useAuthData"


const useFetchData = (url = "", loadDependency) => {

    const [response, setResponse] = useState(null)
    const [isLoading, setIsLoading] = useState(true)
    const [error, setError] = useState(null)
    const { getAuthData } = useAuthData()
    // let dependency = []

    // useEffect(() => {
    //     console.log("usefetch loaded")
    //     fetchData()
    // }, [url])

    // if (loadDependency !== null || loadDependency !== undefined) {
    //     dependency.push(loadDependency)
    // }
    useEffect(() => {
        if ((url != null && url.trim() !== "") || (loadDependency !== null)) {
            console.log("usefetch loaded", loadDependency)
            fetchResponseData()
        }
    }, [url, loadDependency])


    const fetchResponseData = () => {
        console.log("loading for request: ", url)
        const { userToken: token } = getAuthData()
        // const token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbklkIjoiYXlwa2x4MDF2S0l1IiwiY2xhaW0iOnt9LCJleHAiOjE2ODkyMzE3MjQsImlhdCI6MTY4NzkzNTcyNH0.hSO6w3ZXPpMnEtNMsokYXrFSR5obMOWPIbABy6Ecs_0"
        const header = {
            "Content-Type": "application/json",
            "user": "vedanthi@gmail.com",
            "Authorization": `Bearer ${token}`
        }
        axios.get(url, { headers: header }).then(res => {
            console.log("Fetch Resposne for: ", url, " : ", res.data?.success)
            setError(null)
            setResponse(res.data?.success)
        }).catch(err => {
            console.log(err.response.data?.error)
            setResponse(null)
            setError(err.response.data?.error)
        }).finally(() => {
            console.log(error, " : ", response)
            console.log("set Loading to : false")
            setIsLoading(false)
        })
    }

    const fetchData = async (url) => {
        console.log("loading for request: ", url)
        const { userToken: token } = getAuthData()
        // let response;
        // const token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbklkIjoiYXlwa2x4MDF2S0l1IiwiY2xhaW0iOnt9LCJleHAiOjE2ODkyMzE3MjQsImlhdCI6MTY4NzkzNTcyNH0.hSO6w3ZXPpMnEtNMsokYXrFSR5obMOWPIbABy6Ecs_0"
        const header = {
            "Content-Type": "application/json",
            "user": "vedanthi@gmail.com",
            "Authorization": `Bearer ${token}`
        }
        setIsLoading(true)
        return axios.get(url, { headers: header }).then(res => {
            console.log("Fetch Resposne for: ", url, " : ", res.data?.success)
            return res.data?.success
        }).catch(err => {
            console.log(err.response.data?.error)
            return err.response.data?.error
        }).finally(() => {
            setIsLoading(false)
        })
    }

    return { response, isLoading, error, fetchData }

}

export default useFetchData