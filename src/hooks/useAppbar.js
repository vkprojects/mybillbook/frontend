import { useContext, useEffect } from "react"
import AppbarContext from "../context/AppBarProviderContext"

const useAppbar = () => {

    useEffect(() => {
        console.log("use AppbarContext executed")
    }, [])

    return useContext(AppbarContext)
}

export default useAppbar