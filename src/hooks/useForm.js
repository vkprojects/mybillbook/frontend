import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"


const useNewForm = (attributes, error, setAttribute, removeAttribute, setError, removeError) => {
  const [submitValidation, setSubmitValidation] = useState(false)
  const dispatch = useDispatch()

  useEffect(() => {
    console.log("use form loaded")
  }, [])

  // console.log("Object: ", attributes)
  // console.log("Error: ", error)


  const validate = (attributeKey, attributevalue) => {
    // console.log(attributeKey, ":", attributevalue)
    switch (attributeKey) {
      case "email":
        if (!(attributevalue.length > 5)) {
          // console.log("password error")
          dispatch(setError({ [attributeKey]: "Invalid UserName" }))
          break;
        }
        // console.log("username no error")
        dispatch(removeError("email"))
        break;

      case "password":
        // const passRegx = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*?#&]{8,}$');
        // if (!passRegx.test(attributevalue)) {
        //   dispatch(setError({ [attributeKey]: "Invalid Password" }))
        //   break
        // }
        if (!(attributevalue.length >= 5)) {
          dispatch(setError({ [attributeKey]: "Invalid Password" }))
          break
        }
        // console.log("password no error")
        dispatch(removeError("password"))
        break;

      case "transDirection":
        if (attributevalue === "") {
          dispatch(setError({ [attributeKey]: "Enter the value of Debit/Credit" }))
          break;
        }
        dispatch(removeError("transDirection"))
        break;

      case "transactionAmount":
        if (attributevalue === "") {
          dispatch(setError({ [attributeKey]: "Invalid Amount value" }))
          break;
        }
        dispatch(removeError("transactionAmount"))
        break;

      case "transactionBank":
        if (attributevalue === "") {
          dispatch(setError({ [attributeKey]: "Invalid Enter Bank value" }))
          break;
        }
        dispatch(removeError("transactionBank"))
        break;

      case "transactionFrom":
        if (attributevalue === "") {
          dispatch(setError({ [attributeKey]: "Enter to " }))
          break;
        }
        dispatch(removeError("transactionFrom"))
        break;

      case "transactionTo":
        if (attributevalue === "") {
          dispatch(setError({ [attributeKey]: "Invalid Receiver's name" }))
          break;
        }
        dispatch(removeError("transactionTo"))
        break;

      case "transactionType":
        if (attributevalue === "") {
          dispatch(setError({ [attributeKey]: "Enter Transaction way" }))
          break;
        }
        dispatch(removeError("transactionType"))
        break;

      case "transactionDate":
        if (attributevalue === "") {
          dispatch(setError({ [attributeKey]: "Enter Transaction Date" }))
          break;
        }
        dispatch(removeError("transactionDate"))
        break;

      case "accountName":
        if (attributevalue === "") {
          dispatch(setError({ [attributeKey]: "Enter Account Name" }))
          break;
        }
        dispatch(removeError("accountName"))
        break;

      case "accountType":
        if (attributevalue === "") {
          dispatch(setError({ [attributeKey]: "Enter Account Name" }))
          break;
        }
        dispatch(removeError("accountType"))
        break;

      case "accountAdminList":
        if (attributevalue.length > 0) {
          dispatch(setError({ [attributeKey]: "Add Account Admins" }))
          break;
        }
        dispatch(removeError("accountAdminList"))
        break;

      default:
        console.log("default error")
        break;
    }
  }

  // const deleteInputListItem = (attribute, index) => {
  //   console.log("removing from list", index)
  //   dispatch(removeAttribute({ [attribute]: index }))
  // }

  const handleListChange = (attribute, value, action = "push") => {
    console.log(value)
    console.log(attribute)
    if (action === "push") {
      dispatch(setAttribute({ [attribute]: value }))
    }
    else {
      dispatch(removeAttribute({ [attribute]: value }))
    }

    // setInputListItem('')
  }

  const handleSelectChange = (event) => {
    let key = event.target.name
    let value = event.target.value
    console.log(key, "::", value)
    dispatch(setAttribute({ [key]: value }))
  }

  const handleChange = (event, list = false) => {
    event.persist()
    let key = event.target.name
    let value = event.target.value
    dispatch(setAttribute({ [key]: value }))
  }

  const handleValidate = () => {
    // event.preventDefault()
    console.log(attributes)
    for (var key in attributes) {
      if (typeof (attributes[key]) === 'object') {
        if (Array.isArray(attributes[key])) {
          console.log("array")
          validate(key, attributes[key])
        }
        else {
          console.log("Recursion")
          handleValidate(attributes[key])
        }
      }
      else {
        console.log(key, "::", attributes[key])
        validate(key, attributes[key])
      }
    }
    // setSubmitValidation(prevState => !prevState)
    setSubmitValidation(true)

  }

  return { handleChange, handleValidate, handleSelectChange, handleListChange, submitValidation, setSubmitValidation }
}

export default useNewForm