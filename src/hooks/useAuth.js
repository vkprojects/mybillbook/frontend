import React, { useContext, useEffect } from 'react'
import AuthContext from '../context/AuthContext'

const useAuth = () => {

    useEffect(() => {
        console.log("use AuthContext executed")
    }, [])

    return useContext(AuthContext)
}

export default useAuth