import { useDispatch } from "react-redux"
import useAuthData from "./useAuthData"
import axios from "axios"
import { useState } from "react"


export const useFormValidateAndSubmit = ({
    url,
    method,
    formValidation: formValidations,
    errorState: errorState,
    setErrorState: setError

}) => {

    const dispatch = useDispatch()
    const { getAuthData } = useAuthData()
    const [isLoading, setLoading] = useState(false)
    let error = { ...errorState }

    console.log("init Error: ", error)
    /*   
       const handleFormValidate = (attributes) => {
           console.log(attributes)
           for (var key in attributes) {
               if (typeof (attributes[key]) === 'object') {
                   if (Array.isArray(attributes[key])) {
                       console.log("array")
                       validate(dispatch, key, attributes[key])
                   }
                   else {
                       // console.log("Recursion")
                       handleFormValidate(attributes[key])
                   }
               }
               else {
                   console.log(key, "::", attributes[key])
                   validate(dispatch, key, attributes[key])
               }
           }
           // setSubmitValidation(true)
       } 
   */

    // internal methods
    const handleFormSubmit = async (requestBody, isLogin) => {
        console.log("method : ", method)
        console.log("Submitting ReqBody: ", requestBody)
        let result = null;
        const { userToken: token, userName } = getAuthData()
        let requestHeader = {
            "Content-type": "application/json",
            "user": userName,
        }
        if (!isLogin) {
            requestHeader.Authorization = `Bearer ${token}`
        }
        console.log("Request Header:", requestHeader)
        setLoading(true)

        //make method calls depening on required method
        // await axios.post(url, requestBody, { headers: requestHeader })

        await axios({
            method: method,
            url: url,
            headers: requestHeader,
            data: requestBody
        }).then(res => {
            result = res.data
            // set res.
        })
            .catch(err => {
                result = err.response?.data
                // set err.
            })
            .finally(() => {
                setLoading(false)
            })
        return result
    }


    // internal methods
    const handleFormValidate = (attributes, formValidations) => {
        console.log("validating: ", attributes)

        for (var key in attributes) {
            if (typeof (attributes[key]) === 'object') {
                if (Array.isArray(attributes[key])) {
                    console.log(key, "::", attributes[key])
                    console.log("array")
                    attributes[key].map((listValues, index) => {
                        if (typeof (listValues) === 'object') {
                            handleFormValidate(listValues, formValidations)
                        }
                    })
                    if (!formValidations[key]?.validation) {
                        console.log("Setting Error: ", formValidations[key]?.message)
                        error = { ...error, [key]: formValidations[key]?.message }
                    }
                    else {
                        delete error[key]
                    }
                }
            }
            else {
                console.log(key, "::", attributes[key], "::", !formValidations[key]?.validation)
                if (!formValidations[key]?.validation(attributes[key])) {
                    console.log("Setting Error: ", formValidations[key]?.message)
                    error = { ...error, [key]: formValidations[key]?.message }
                }
                else {
                    delete error[key]
                }
            }
        }
        return error;

    }


    // exposed methods
    const handleValidateAndSubmit = async (formFields, login = false) => {
        if (method == null) return;

        console.log("handleValidateAndSubmit : ")
        const error = handleFormValidate(formFields, formValidations)
        let responseBody = null;

        dispatch(setError({ ...error }))
        if (Object.keys(error).length === 0) {
            responseBody = await handleFormSubmit(formFields, login)
        }
        console.log("Response : ", responseBody)
        return responseBody
    }

    const validateField = (key, value) => {
        console.log("key : ", key)
        const attribute = formValidations[key]

        if (!attribute?.validation(value)) {
            console.log("setting error: ", key, ": ", attribute?.message)
            dispatch(setError({ [key]: attribute?.message, ...error }))
        } else {
            const { [key]: removedErrorField, ...errorFields } = error
            console.log("removing and setting other attribute", errorFields)
            dispatch(setError({ ...errorFields }))
        }
    }

    const handleValidateField = (e) => {
        console.log("validating", e.target.value)
        let attributeName = e.target.name
        let attributeValue = e.target.value
        validateField(attributeName, attributeValue)
    }

    return { handleValidateAndSubmit, handleValidateField, isLoading }
}


export const useFormFieldsHandle = ({ setFieldAttribute, setFieldAttributes = () => { }, removeFieldAttribute = {}, resetFieldAttributes = () => { } }) => {
    const dispatch = useDispatch()

    const handleTextChange = (event) => {
        event.persist()
        let key = event.target.name
        let value = event.target.value
        console.log("handleTextChange: ", key, " ", value)

        dispatch(setFieldAttribute({ [key]: value }))
    }

    const handleSelectChange = (event) => {
        let key = event.target.name
        let value = event.target.value
        console.log("handleSelectChange ", key, "::", value)
        dispatch(setFieldAttribute({ [key]: value }))
    }

    const handleListChange = (attribute, value, action = "push") => {
        console.log("handleListChange: ", value, " ", attribute)
        if (action === "push") {
            dispatch(setFieldAttribute({ [attribute]: value }))
        }
        else {
            dispatch(removeFieldAttribute({ [attribute]: value }))
        }
        // setInputListItem('')
    }
    const handleSetAllFields = (fieldObj) => {
        console.log("handleSetAllFields: ", fieldObj)
        dispatch(setFieldAttributes(fieldObj))
    }

    const resetAllAttributes = () => {
        console.log("remoivng")
        dispatch(resetFieldAttributes())
    }

    return { handleTextChange, handleSelectChange, handleListChange, handleSetAllFields, resetAllAttributes }
}

/* 
export const useFormSubmit = (url, requestBody) => {
    const { getAuthData } = useAuthData()

    const handleSubmit = async (login = false) => {
        console.log("ReqBody: ", requestBody)
        const { userToken: token, userName } = getAuthData()
        let requestHeader = {
            "Content-type": "application/json",
            "user": userName,
        }
        if (!login) {
            requestHeader.Authorization = `Bearer ${token}`
        }
        console.log("Request Header:", requestHeader)
    }

    return { handleSubmit }
}
 */