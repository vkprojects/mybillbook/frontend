import { useEffect } from "react"
import { useCookies } from 'react-cookie';

const useAuthData = () => {

    const [userCookie, setUserCookie, removeUserCookie] = useCookies(['user']);

    // useEffect(() => {
    //     console.log("use Authdata loaded")
    // }, [])

    // const [username, setUsername] = useState(second)
    const storageKeys = {
        // "sessionLoginStatus": "sessionLoginStatus",
        "userToken": "userToken"
    }

    const setAuthAuthData = (token, user) => {
        console.log("setting storage", token, user)
        const currrentDate = new Date()
        currrentDate.setDate(currrentDate.getDate() + 15)
        console.log("date :" + currrentDate)
        setUserCookie('user', user, { path: '/', expires: currrentDate });
        // localStorage.setItem(storageKeys.sessionLoginStatus, true)
        localStorage.setItem(storageKeys.userToken, token)
    }

    const removeAuthAuthdata = () => {
        console.log("clearing storage")
        removeUserCookie('user', { path: '/' })
        // localStorage.removeItem(storageKeys.sessionLoginStatus)
        localStorage.removeItem(storageKeys.userToken)
    }

    const getAuthData = () => {
        const sessionData = {
            // "loginStatus": localStorage.getItem(storageKeys.sessionLoginStatus),
            "userToken": localStorage.getItem(storageKeys.userToken),
            "userName": userCookie.user
        }
        return sessionData
    }
    return { setAuthAuthData, getAuthData, removeAuthAuthdata }
}

export default useAuthData