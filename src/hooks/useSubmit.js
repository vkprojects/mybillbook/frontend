import axios from 'axios'
import { useState } from 'react'
import { Navigate, useNavigate } from 'react-router-dom'
import useAuthData from './useAuthData'

const useFormSubmit = (reqUrl, reqBody) => {
    const [isPending, setIsPending] = useState(false)
    const [response, setResponse] = useState(null)
    const [errorResponse, setErrorResponse] = useState(null)
    const { getAuthData } = useAuthData()
    console.log("useFormSubmit loaded")

    const handleSubmit = async (login = false) => {
        console.log("Calling Submit function")
        console.log("Req: ", reqBody)
        const { userToken: token } = getAuthData()
        let requestHeader = {
            "Content-type": "application/json",
            "user": "x9@gmail.com",
        }
        if (!login) {
            console.log("Token: ", token)
            requestHeader.Authorization = `Bearer ${token}`
            // const requestHeader = {
            //     "Content-type": "application/json",
            //     "user": "x9@gmail.com",
            //     // "Authorization": `Bearer ${token}`
            // }
        }
        console.log("Header", requestHeader)
        await axios.post(reqUrl, reqBody, { headers: requestHeader })
            .then(res => {
                console.log("Submit Response: ", res)
                if (res.status === 200) {
                    setResponse(res.data.success)
                }
                else {
                    console.log("Then Err: ", res)
                    setErrorResponse(res?.data?.error)
                }
            })
            .catch(err => {
                console.log("Catch Err: ", err)
                if (err.status === 401) {
                    setResponse(null)
                }
                setErrorResponse(err.response?.data?.error)
            })


        // console.log("Req has error: ", error)


        // setIsPending(true)
        // if (!reqBody.hasOwnProperty("error")) {
        //     axios.post(reqUrl, reqBody).then((res) => {
        //         console.log("Response: ", res)
        //     }).catch(err => {
        //         console.log("resposne error :", err)
        //     })
        // }
        // setIsPending(false)
    }

    const handleSubmitOnClick = async (request, url) => {
        console.log("Hi I submit only when u click submit..........")
    }

    return { isPending, response, setIsPending, handleSubmit, handleSubmitOnClick, errorResponse, }
}

export default useFormSubmit