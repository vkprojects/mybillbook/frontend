import { useDispatch } from "react-redux"
import { setBillAccountError as setError, removeError, resetError } from "../redux/billAccountsSlice"

export const billAccountFormValidation = (dispatch, attributeKey, attributevalue) => {
    // const dispatch = useDispatch()

    switch (attributeKey) {
        case "accountName":
            console.log(attributevalue)
            if (attributevalue.length < 4) {
                dispatch(setError({ [attributeKey]: "Lenght of this field should be atleast 5" }))
                break;
            }
            dispatch(removeError("accountName"))
            break;

        case "accountType":
            if (attributevalue === "") {
                dispatch(setError({ [attributeKey]: "Select Account Type" }))
                break;
            }
            dispatch(removeError("accountType"))
            break;

        case "accountAdminList":
            if (attributevalue.length < 1) {
                dispatch(setError({ [attributeKey]: "Add Account Admins" }))
                break;
            }
            dispatch(removeError("accountAdminList"))
            break;
        case "accountUserList":
            if (attributevalue.length < 1) {
                dispatch(setError({ [attributeKey]: "Add Account User" }))
                break;
            }
            dispatch(removeError("accountUserList"))
            break;

        default:
            console.log("default error")
            break;
    }
}