export const transactionFormValidation = {
    "transactionFrom": {
        "validation": (value) => value.length > 4,
        "message": "Required Account Name"
    },
    "transactionAmount": {
        "validation": (value) => value > 0,
        "message": "Required Account Transaction Amount"
    },
    "transactionTo": {
        "validation": (value) => value.length > 0,
        "message": "Required Account transactionTo"
    },
    "transactionDate": {
        "validation": (value) => value.length > 0,
        "message": "Required Account transactionDate"
    },
    "transactionBank": {
        "validation": (value) => value.length > 0,
        "message": "Required Transaction Bank"
    },
    "transactionType": {
        "validation": (value) => value.length > 0,
        "message": "Required Transaction Type"
    },
    "transDirection": {
        "validation": (value) => value.length > 0,
        "message": "Required Transaction Direction"
    },
    "description": {
        "validation": (value) => true,
        "message": null
    },
    "transactionList": {
        "validation": (value) => value.length > 0,
        "message": "Required Account User"
    },
    "billAccountId": {
        "validation": (value) => value.length > 0,
        "message": "Required Account User"
    }

}