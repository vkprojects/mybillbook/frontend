export const createAccountValidation = {
    "accountName": {
        "validation": (value) => value.length > 4,
        "message": "Required Account Name"
    },
    "accountAdminList": {
        "validation": (value) => value.length > 0,
        "message": "Required Account Admin"
    },
    "accountType": {
        "validation": (value) => value.length > 0,
        "message": "Required Account Type"
    },
    "accountUserList": {
        "validation": (value) => value.length > 0,
        "message": "Required Account User"
    },
    "billAccountId": {
        "validation": (value) => true,
        "message": null
    }
}