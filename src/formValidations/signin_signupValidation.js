export const signin_signupValidation = {
    "email": {
        "validation": (value) => value.length > 4,
        "message": "Required email"
    },
    "password": {
        "validation": (value) => value.length > 5,
        "message": "Required password"
    }
}