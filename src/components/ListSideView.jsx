import { Drawer, List, ListItem, ListItemButton, ListItemIcon, ListItemText, Toolbar } from '@mui/material'
import { Box } from '@mui/system'
import React, { useState } from 'react'
import ManageAccountsIcon from '@mui/icons-material/ManageAccounts';
import { ContactEmergency, Person, Settings, SupervisorAccount } from '@mui/icons-material';
// import { ManageAccounts } from './ManageAccounts';
import Profile from './Profile';
import SettingsView from './SettingsView';
import AccountTransactionView from './AccountTransactionView';
import { useTheme } from '@mui/styles';
import { pageItems } from "../data/PageItems"

const ListSideView = ({ currentIndex, setCurrentIndex }) => {

    const theme = useTheme()
    // const listItems = [{"Manage Bill Accounts"}, "Settings", "Profile", "Contacts"]

    const ListView = () => (
        <List sx={{
            height: "100%",
            color: "white",
            backgroundColor: theme.palette.primary.main,
            borderRadius: "0 10px 10px 0"
        }}    >
            {pageItems.map(({ label, icon, page }, index) => {
                // console.log(page === currentPage)
                return (

                    <ListItem key={index} sx={{ padding: 'inherit', opacity: index === currentIndex ? 1 : 0.7 }}>
                        <ListItemButton
                            selected={currentIndex === index}
                            onClick={(e) => { setCurrentIndex(index) }}>
                            <ListItemIcon sx={{ justifyContent: "center", color: "white" }}>{icon}</ListItemIcon>
                            <ListItemText
                                sx={{
                                    display: {
                                        mobile: 'none',
                                        tablet: "inline"
                                    }
                                }}
                            >{label}</ListItemText>
                        </ListItemButton>
                    </ListItem>
                )
            }
            )}
        </List>
    )

    return (
        // <div>
        // <Drawer open={true}
        //     variant="permanent"
        //     sx={{
        //         width: "200px",
        //         // [`& .MuiDrawer-paper`]: { width: "200px", boxSizing: 'border-box' },
        //     }}
        //     PaperProps={{
        //         sx: {
        //             // height: "70%",
        //             // insetBlock: "20%",
        //             justifyContent: "center",
        //             alignContent: "center"
        //         }
        //     }}
        // >
        // <Box sx={{ display: "block" }}>
        // {/* <Toolbar /> */ }
        < ListView />
        // </Box>
        // {/* </Drawer > */ }
        // </div>
    )
}

export default ListSideView