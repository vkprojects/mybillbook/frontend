import React, { useState } from 'react'
import { Outlet } from 'react-router-dom'
import MainAppBar from './MainAppBar'

const RequiredAppBar = () => {
    const [currentIndex, setCurrentIndex] = useState(0)
    return (
        <>
            <MainAppBar currentIndex={currentIndex} setCurrentIndex={setCurrentIndex} />
            <Outlet />
        </>
    )
}

export default RequiredAppBar