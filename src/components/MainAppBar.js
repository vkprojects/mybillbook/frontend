import { AppBar, Box, Button, Drawer, IconButton, List, ListItem, ListItemIcon, ListItemText, Menu, MenuItem, MenuList, Toolbar, Typography } from '@mui/material';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { pageItems } from '../data/PageItems';
import useAppbar from '../hooks/useAppbar';
import useAuth from '../hooks/useAuth';
import MenuIcon from '@mui/icons-material/Menu';

const MainAppBar = () => {

    const { setAuth } = useAuth()
    const navigate = useNavigate()

    const handleLogout = (e) => {
        e.preventDefault()
        setAuth(false)
        navigate("/login")
    }

    return (
        <>
            <AppBar component="nav"
                position="fixed"
                sx={{ zIndex: (theme) => theme.zIndex.drawer + 1 }}
            >
                <Toolbar>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        My Bill Book
                    </Typography>
                    <LargeScreenView handleLogout={handleLogout} />
                    <SmallScreenSizeView handleLogout={handleLogout} />
                </Toolbar>
            </AppBar>

        </>
    )
}

const LargeScreenView = ({ handleLogout }) => {

    return (
        <Box sx={{
            display: {
                mobile: "none",
                laptop: "flex"
            },
            justifyContent: "right"
        }}>
            {pageItems.map((value, key) => {
                return (
                    <AppBarItem value={value} key={key} index={key} />
                )

            })}
            <Button
                variant="body2"
                onClick={handleLogout}
                sx={{
                    color: "inherit",
                    marginInline: 2
                }}
            >
                LOGOUT
            </Button>
        </Box>
    )
}

const SmallScreenSizeView = ({ handleLogout }) => {

    const [anchorEl, setAnchorEl] = useState(null)
    const open = Boolean(anchorEl)

    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget)
    }
    const handleClose = () => {
        setAnchorEl(null);
    };


    return (
        <Box
            sx={{
                display: {
                    mobile: "flex",
                    laptop: "none"
                },
            }}
        >
            <IconButton
                color='inherit'
                id="basic-button"
                aria-controls={open ? 'basic-menu' : undefined}
                aria-haspopup="true"
                aria-expanded={open ? 'true' : undefined}
                onClick={handleMenu}
            >
                <MenuIcon />
            </IconButton>
            <Menu
                id="basic-menu"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                MenuListProps={{
                    'aria-labelledby': 'basic-button',
                }}
                sx={{
                    display: {
                        mobile: "flex",
                        laptop: "none"
                    },
                }}
            >
                {pageItems.map((value, key) => {
                    return (
                        <MenuItem key={key} onClick={handleClose}>
                            <AppBarItem value={value} index={key} />
                        </MenuItem>
                    )

                })}
                <MenuItem onClick={handleClose}>
                    <Button
                        variant="body2"
                        onClick={handleLogout}
                        sx={{
                            color: "inherit",
                            marginInline: {
                                laptop: 2
                            },
                        }}
                    >
                        LOGOUT
                    </Button>
                </MenuItem>
            </Menu>
        </Box>
    )
}

const AppBarItem = ({ value, index }) => {

    const { currentIndex, setCurrentIndex } = useAppbar()
    console.log("menu:", value.label)

    return (
        <Button
            onClick={(e) => { setCurrentIndex(index) }}

            variant="body2"
            sx={{
                color: "inherit",
                marginInline: {
                    laptop: 2
                },
                boxShadow: currentIndex === index ? "inset 0 0 1px 1px  white" : "none"
            }}
        >
            {value.label}
        </Button>
    )
}




export default MainAppBar