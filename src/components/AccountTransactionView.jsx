import React, { useMemo, useState, useEffect } from 'react'
import { AgGridReact } from 'ag-grid-react'
import { useSelector } from 'react-redux'
import 'ag-grid-community/styles/ag-grid.css';
import 'ag-grid-community/styles/ag-theme-alpine.css';
import "../styles/accountTransactionView.css"
import { makeStyles, useTheme } from '@mui/styles';
import ArrowCircleUpTwoToneIcon from '@mui/icons-material/ArrowCircleUpTwoTone';
import ArrowCircleDownTwoToneIcon from '@mui/icons-material/ArrowCircleDownTwoTone';
import { Box } from '@mui/system';
import { Button, FormControl, Grid, InputLabel, MenuItem, Select, Typography } from '@mui/material';
import { Add } from '@mui/icons-material';
import TransactionEntryModal from './modals/TransactionEntryModal';
import { getAllTransactions } from "../apis/UserServices"
import useFetchData from '../hooks/useFetchData';
import { BASE_URL } from '../constants';



// import CreateAccountBtn from './CreateAccountBtn';




const AccountTransactionView = ({ accountList }) => {

    console.log(accountList[0].accountId)
    const [accountId, setAccountId] = useState(accountList[0].accountId)
    // let accountId = "643e976e7c54522f5badcabd"
    localStorage.setItem("activeAccount", accountId);
    // const { data, error, loading } = useFetchData("http://localhost:8080/transaction/getAllTransactions/6440b7445a04fd4bfb965673")

    const [loadComponent, setLoadComponent] = useState(false)
    const [openTransactionFormState, setTransactionFormState] = useState(false)
    const { response: transactionListResponse, isLoading, error, fetchData } = useFetchData(`${BASE_URL}/transaction/getAllTransactions/${accountId}`, loadComponent)
    const { details: transactionList } = isLoading ? [] : transactionListResponse;
    console.log(isLoading)
    console.log(transactionList)


    // useEffect(() => {
    //     fetchData()
    // }, [open])


    const [columnDefs] = useState([


        {
            headerName: 'In/Out',
            field: 'transDirection',
            cellStyle: { 'borderRight': '1px solid', "display": "flex", 'alignItems': 'center' },
            minWidth: 50,
            maxWidth: 80,
            cellRenderer: (transaction) => {
                if (transaction.data.transDirection === "In") {
                    return <ArrowCircleDownTwoToneIcon sx={{ color: "green" }} />
                }
                else {
                    return <ArrowCircleUpTwoToneIcon sx={{ color: "red" }} />
                }
            }
        },
        {
            headerName: 'Date', field: 'transactionDate', maxWidth: 150,

            cellRenderer: (transaction) => {
                // return transaction.data.transactionDate.split("T")[0]
                return transaction.data.transactionDate.substr(0, 10)
            },
            responsive: true
        },
        { headerName: 'Type', field: 'transactionType' },
        {
            headerName: 'Amount',
            field: 'transactionAmount',
            cellRenderer: (transaction) => {
                if (transaction.data.transDirection === "In") {
                    return <Box sx={{ color: "green" }}>{transaction.data.transactionAmount}</Box>
                }
                else {
                    return <Box sx={{ color: "red" }}>{transaction.data.transactionAmount}</Box>
                }
            }
        },
        { headerName: 'User', minWidth: 150, field: 'transactionFrom' },
        { headerName: 'Bank', field: 'transactionBank' }

    ]);

    // console.log(transactions[0])
    const defaultColDef = useMemo(() => {
        return {
            flex: 1,
            minWidth: 100,
            boxShadow: "5px 5px 10px 0px hsl(269deg 100% 50% / 48%)",
            // width: 150,
            // enableRowGroup: true,
            // enablePivot: true,
            // enableValue: true,
            sortable: true,
            filter: true,
            // editable: true,
        };
    }, []);


    if (isLoading) {
        console.log("loading....")
        return <h1>Loading...</h1>
    }
    else if (!(!!transactionList)) {
        console.log("No data Found")
        return <h1>No data Found</h1>
    }
    else {
        var gridOptions = {
            responsiveMinWidth: 300,
            rowStyle: {
                // color: "white",
                // boxShadow: "hsl(0deg 0% 50% / 50%) 0px 0px 15px inset"
                // boxShadow: "hsl(0deg 100% 99%) 0px 10px 15px 0px inset"
            },

            //styling rown on condition
            // rowClassRules: {
            //     "row-green": transaction => transaction.api.getValue("transDirection", transaction.node) === "In",
            //     "row-red": transaction => transaction.api.getValue("transDirection", transaction.node) === "Out"
            // }
        }

        function getBalance() {
            const balance = transactionList.length != 0 && transactionList?.reduce((prev, curr) => {
                console.log(curr.transDirection)
                if (curr.transDirection === "In") {
                    return { transactionAmount: parseInt(prev.transactionAmount) + parseInt(curr.transactionAmount) }

                }
                else {
                    return { transactionAmount: parseInt(prev.transactionAmount) - parseInt(curr.transactionAmount) }

                }
            })

            console.log(balance)

            return balance ? balance.transactionAmount : "--"
        }

        // function getBalance() {
        //     const balance = transactionList?.reduce((prev, curr) => (
        //         { transactionAmount: parseInt(prev.transactionAmount) + parseInt(curr.transactionAmount) }
        //     ))

        //     return balance.transactionAmount
        // }

        // var balance = transactionList?.reduce((prev, curr) => (parseInt(prev.transactionAmount) + parseInt(curr.transactionAmount)))
        // console.error(balance)


        const handleTransModal = () => {
            // e.preventDefault()
            console.log("handleTransModal")
            setTransactionFormState(false)
            setLoadComponent(prev => !prev)
        }

        return (
            <div style={{
                height: "75%",
                // margin: "2%",
                flex: 1
            }}>
                <Grid container sx={{ marginBlock: "15px" }}>
                    <Grid item mobile={4} tablet={2}>
                        {/* <Typography mobile={6} variant='p' sx={{ fontWeight: 'bold' }}>Current Viewing</Typography> */}
                        <ChooseAccountDropDown initAccountId={accountId} setAccountId={setAccountId} accountList={accountList} />
                    </Grid>
                    <Grid item mobile={8} tablet={10}>
                        {!isLoading && <Typography variant='h5' sx={{ textAlign: "right" }}> {`Balance : ${getBalance()}`} </Typography>}
                    </Grid>
                </Grid>

                {!isLoading &&
                    <AgGridReact
                        className="ag-theme-alpine"
                        columnDefs={columnDefs}
                        rowData={transactionList}
                        gridOptions={gridOptions}
                        defaultColDef={defaultColDef}
                    />}
                <AddTransactionButton setTransactionFormState={setTransactionFormState} />
                {openTransactionFormState && <TransactionEntryModal open={openTransactionFormState} handleClose={handleTransModal} />}
                {/* <NewTransactionModel open={open} handleClose={handleTransModal} /> */}


            </div>
        )
    }
}

const ChooseAccountDropDown = ({ initAccountId, setAccountId, accountList }) => {
    console.log(initAccountId)

    let initAccount = accountList.filter((account, key) => account.accountId === initAccountId)
    // console.log(initAccount[0].accountName)

    // const [{ accountId: firstAccount }] = initAccount;

    const [accountName, setAccountName] = useState(initAccount[0].accountName)

    console.log(accountName)
    // useEffect(() => {
    //     first

    // }, [third])

    const handleSelectChange = (event) => {
        let accountId = event.target.value
        setAccountId(accountId)
    }

    return (
        <FormControl fullWidth>
            {/* <InputLabel id="Accounts">Accounts</InputLabel> */}
            <Select
                variant='standard'
                labelId="Accounts"
                label="Accounts"
                defaultValue={initAccountId}
                value={initAccountId}
                onChange={handleSelectChange}
            >{accountList.map((account, key) => {
                // console.log(account, key)
                return (<MenuItem key={key} value={account?.accountId}>{account.accountName}</MenuItem>)
            })}
                {/* <MenuItem value="Name 1">Name 1</MenuItem>
                <MenuItem value="Name 2">name 2</MenuItem> */}
            </Select>
        </FormControl>
    )
}

const AddTransactionButton = ({ setTransactionFormState }) => {

    return (
        <Button
            onClick={() => { setTransactionFormState(true) }}
            // color="primary"
            variant="contained"
            sx={{
                position: "fixed",
                bottom: '5%',
                right: "5%",
                minWidth: 'unset',
                // width: "50px",
                // height: "50px",
                // borderRadius: "50%",
            }}
        >
            New Transaction
            {/* <Add sx={{ color: "white" }} /> */}
        </Button>
    )
}






export default AccountTransactionView