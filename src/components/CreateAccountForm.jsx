import React, { useEffect, useState } from 'react'
import { useFormFieldsHandle } from '../hooks/useFormValidateSubmit'
import { Button, Chip, Grid, MenuItem, Paper, Typography } from '@mui/material'
import { GridRowSection, StyledTextField } from '../styles/login/styles'
import { Add } from '@mui/icons-material'
import { useDispatch, useSelector } from 'react-redux'
import { useCookies } from 'react-cookie'
import { removeBillAccountAttribute, resetAttribute, setBillAccountAttribute, setBillAccountAttributes } from '../redux/billAccountsSlice'

// import useNewForm from '../../hooks/useForm'


const CreateAccountForm = (props) => {

    const billAccount = useSelector(state => state.billAccount.attributes)
    const error = useSelector(state => state.billAccount.error)

    const dispatch = useDispatch()
    const [{ user: activeUser }] = useCookies(['user']);

    const [inputAdminListItem, setInputAdminListItem] = useState('')
    const [inputUserListItem, setInputUserListItem] = useState('')
    const { billAccountDetail, handleSubmit, handleValidateField } = props
    // const { billAccount, setBillAccountAttribute, setBillAccountAttributes, removeBillAccountAttribute, resetAttribute, error, handleSubmit, handleValidateField } = props

    const { handleTextChange, handleListChange, handleSelectChange, handleSetAllFields, resetAllAttributes } = useFormFieldsHandle({
        setFieldAttribute: setBillAccountAttribute,
        setFieldAttributes: setBillAccountAttributes,
        removeFieldAttribute: removeBillAccountAttribute,
        resetFieldAttributes: resetAttribute
    })

    useEffect(() => {
        console.log("setting attribute :", billAccountDetail)
        if (!!billAccountDetail) {
            console.log("setting attribute :", billAccountDetail)
            handleSetAllFields({ ...billAccountDetail })
        }
        else {
            resetAllAttributes()
            handleAdminList(activeUser)
        }
    }, [])




    // const handleNewBillAccountSubmit = async (e) => {
    //     e.preventDefault()
    //     const response = await handleValidateAndSubmit(billAccount)
    //     console.log("component rsponse: ", response)

    //     if (response.error === null) {
    //         handleClose()
    //     }
    //     else {
    //         console.log(response.status === 400)
    //         if (response.status === 400) {
    //             console.log("Setting Error: ", response.error?.details)
    //             dispatch(setBillAccountError({ ...response.error?.details }))
    //         }
    //     }

    // }


    // useEffect(() => {
    //   first
    //   return () => {
    //     resetAllAttributes()
    // }
    // }, [])

    // useEffect(() => {
    //     console.log("state", billAccount)
    //     // console.log("User id:", activeUser, billAccountId)
    //     handleAdminList(activeUser)

    // }, [])

    // useEffect(() => {
    //     console.log("-->", response)
    //     if (response != null) {
    //         console.log("-->", response?.details[0])
    //         handleSetAllFields(response)
    //     }

    // }, [fetchDataLoading])

    // useEffect(() => {

    // }, [])



    const handleAdminList = (value) => {
        console.log("Adding Admin value: ", value)
        handleListChange("accountAdminList", value)
        handleListChange("accountUserList", value)
    }


    return (

        <>
            {console.log("Setting Data: ", billAccount)}
            <Paper sx={{
                width: {
                    mobile: "80vw",
                    tablet: "70vw",
                    laptop: "60vw"
                },
                height: "max-content",
                padding: "5%",
                // textAlign: 'center'
                // backgroundColor: "red"
            }}>

                <Typography variant='h6' align='center' color="secondary"
                    sx={{
                        marginBottom: 2,
                        borderBottom: "2px solid red",
                        width: "50%",
                        marginInline: 'auto'
                    }}
                >New Bill Account</Typography>
                <form onSubmit={handleSubmit}>
                    <Grid container mobile={12} paddingTop={3}

                    >
                        <GridRowSection>
                            <StyledTextField
                                variant='standard'
                                fieldwidth={5}
                                fieldlabel="Account Name"
                                id="accountName"
                                name="accountName"
                                value={billAccount.accountName}
                                onChange={handleTextChange}
                                onBlur={handleValidateField}
                                error={!!error.accountName}
                                helperText={!!error.accountName ? error.accountName : ""}
                            />
                            <StyledTextField
                                variant='standard'
                                fieldwidth={5}
                                fieldlabel="Account Admins"
                                id="accountAdminList"
                                name="accountAdminList"
                                // value={billAccountAttribute.accountAdminList}
                                value={inputAdminListItem}
                                onBlur={handleValidateField}
                                onChange={(e) => { setInputAdminListItem(e.target.value) }}
                                error={!!error.accountAdminList}
                                helperText={!!error.accountAdminList && error.accountAdminList}
                                InputProps={{
                                    endAdornment:
                                        <Button
                                            sx={{ padding: 0, minWidth: 0 }}
                                            onClick={() => {
                                                handleAdminList(inputAdminListItem)
                                                setInputAdminListItem("")
                                                // setInputUserListItem("")
                                            }}
                                        >
                                            <Add />
                                        </Button>
                                }}
                            />
                        </GridRowSection>
                        <GridRowSection sx={{ justifyContent: 'right' }}>
                            {!!billAccount.accountAdminList &&
                                billAccount.accountAdminList.map((email, index) => (
                                    <Chip
                                        key={index}
                                        label={email}
                                        color="primary"
                                        variant="outlined"
                                        disabled={index === 0}
                                        onDelete={(e) => {
                                            handleListChange("accountAdminList", index, "pop")
                                        }}
                                        // {(e) => { deleteInputListItem("accountAdminList", index) }}
                                        sx={{
                                            margin: 1
                                        }}
                                    />
                                ))
                            }
                        </GridRowSection>
                        <GridRowSection>
                            <StyledTextField
                                select
                                variant='standard'
                                fieldwidth={5}
                                fieldlabel="Account Type"
                                id="accountType"
                                name="accountType"
                                value={billAccount.accountType}
                                onChange={handleSelectChange}
                                error={!!error.accountType}
                                helperText={!!error.accountType ? error.accountType : ""}
                                onBlur={handleValidateField}
                            >
                                <MenuItem value="Home">Home</MenuItem>
                                <MenuItem value="Business">Business</MenuItem>
                                <MenuItem value="Others">Others</MenuItem>
                            </StyledTextField>

                            <StyledTextField
                                variant='standard'
                                fieldwidth={5}
                                fieldlabel="Account Users"
                                id="accountUserList"
                                name="accountUserList"
                                // value={billAccountAttribute.accountAdminList}
                                value={inputUserListItem}
                                onChange={(e) => { setInputUserListItem(e.target.value) }}
                                error={!!error.accountUserList}
                                helperText={!!error.accountUserList ? error.accountUserList : ""}
                                onBlur={handleValidateField}
                                InputProps={{
                                    endAdornment:
                                        <Button
                                            sx={{ padding: 0, minWidth: 0 }}
                                            onClick={() => {
                                                handleListChange("accountUserList", inputUserListItem)
                                                setInputUserListItem("")
                                            }}
                                        >
                                            <Add />
                                        </Button>
                                }}
                            />
                        </GridRowSection>
                        <GridRowSection sx={{ justifyContent: 'right' }}>
                            {!!billAccount.accountUserList &&
                                billAccount.accountUserList.map((mail, index) => (
                                    <Chip
                                        key={index}
                                        label={mail}
                                        color="primary"
                                        variant="outlined"
                                        disabled={!!billAccount.accountAdminList && billAccount.accountAdminList.includes(mail)}
                                        onDelete={(e) => {
                                            handleListChange("accountUserList", index, "pop")

                                        }}
                                        // {(e) => { deleteInputListItem("accountAdminList", index) }}
                                        sx={{
                                            margin: 1
                                        }}
                                    />
                                ))
                            }
                        </GridRowSection>
                        <GridRowSection sx={{ justifyContent: "right" }} gap={2}>
                            <Button
                                variant='outlined'
                            >Reset</Button>
                            <Button
                                type='submit'
                                variant='contained'
                            >Submit</Button>
                        </GridRowSection>
                    </Grid>
                </form>
            </Paper>
        </>
    )
}

export default CreateAccountForm