import { Button, Chip, Dialog, FormControl, FormHelperText, Grid, InputLabel, MenuItem, Paper, Select, TextField, Typography } from '@mui/material'
import React, { useEffect, useState } from 'react'
// import useFormSubmit from '../../hooks/useSubmit'
import { setBillAccountAttribute, setBillAccountAttributes, removeBillAccountAttribute, setBillAccountError, removeError, resetError, resetAttribute } from "../../redux/billAccountsSlice"
import { useDispatch, useSelector } from 'react-redux'
import { GridRowSection, StyledTextField } from '../../styles/login/styles'
import { useCookies } from 'react-cookie'
// import useNewForm from '../../hooks/useForm'
import { Add } from '@mui/icons-material'
import { billAccountFormValidation } from "../../formValidations/billAccountValidation"
import { useFormFieldsHandle, useFormValidateAndSubmit } from "../../hooks/useFormValidateSubmit"
import { createAccountValidation } from '../../formValidations/createAccountValidation'
import useFetchData from '../../hooks/useFetchData'
import CreateAccountForm from '../CreateAccountForm'
import { BASE_URL } from '../../constants'

const AddAccountModal = ({ open, handleClose, billAccountDetail }) => {
    const billAccountAttribute = useSelector(state => state.billAccount.attributes)
    const error = useSelector(state => state.billAccount.error)
    // let jsonBillAccountAttribute = JSON.parse(billAccountAttribute)
    // const { handleChange, handleValidate: handleAccountAttributes, handleListChange, handleSelectChange } = useNewForm(billAccountAttribute, error, setBillAccountAttribute, removeBillAccountAttribute, setBillAccountError, removeError)
    // const { isPending, response, setIsPending, handleSubmit, handleSubmitOnClick, errorResponse } = useFormSubmit("http://localhost:8080/account/create", billAccountAttribute)
    const [{ user: activeUser }] = useCookies(['user']);
    // const [inputAdminListItem, setInputAdminListItem] = useState('')
    // const [inputUserListItem, setInputUserListItem] = useState('')

    // const { response, isLoading: fetchDataLoading, errorL } = useFetchData(`http://localhost:8080/account/search?accountId=${billAccountId}`, billAccountId)

    // const { handleFormValidate, handleValidateField } = useFormValidate(billAccountFormValidation)

    const { handleValidateAndSubmit, handleValidateField, isLoading } = useFormValidateAndSubmit({
        url: billAccountDetail == null ? `${BASE_URL}/account/create` : `${BASE_URL}/account/updateMetadata`,
        method: billAccountDetail == null ? "post" : "put",
        formValidation: createAccountValidation,
        errorState: error,
        setErrorState: setBillAccountError
    })

    // const { handleTextChange, handleListChange, handleSelectChange, handleSetAllFields, resetAllAttributes } = useFormFieldsHandle({
    //     setFieldAttribute: setBillAccountAttribute,
    //     setFieldAttributes: setBillAccountAttributes,
    //     removeFieldAttribute: removeBillAccountAttribute,
    //     resetFieldAttributes: resetAttribute
    // })

    const dispatch = useDispatch()

    const handleNewBillAccountSubmit = async (e) => {
        e.preventDefault()
        let requestBody;
        if (!!billAccountDetail) {
            requestBody = {
                "billAccountId": billAccountDetail.accountId,
                "billAccount": billAccountAttribute
            }
        }
        else {
            requestBody = billAccountAttribute

        }
        console.log("component request: ", requestBody)

        // const requestBody  = billAccountDetail == null ? billAccountAttribute: ""
        const response = await handleValidateAndSubmit(requestBody)
        console.log("component rsponse: ", response)

        if (response.error === null) {
            handleClose()
            dispatch(resetAttribute())
        }
        else {
            console.log(response.status === 400)
            if (response.status === 400) {
                console.log("Setting Error: ", response.error?.details)
                dispatch(setBillAccountError({ ...response.error?.details }))
            }
        }

    }


    // useEffect(() => {
    //     console.log("-->", response, "loading: ", fetchDataLoading)
    //     if (response != null) {
    //         console.log("-->", response?.details[0])
    //         // handleSetAllFields(response?.details[0])
    //     }

    // }, [fetchDataLoading])

    // useEffect(() => {
    //     console.log("-->", "Form filled attribute: ", billAccountAttribute)

    // }, [billAccountAttribute])







    useEffect(() => {
        console.log("sending attribute :", billAccountDetail)
    }, [])


    return (
        <Dialog
            open={open}
            onClose={handleClose}
        >
            {(isLoading) ? <h3>Loading...</h3> :
                !!billAccountAttribute ?
                    <>
                        <CreateAccountForm
                            billAccountDetail={billAccountDetail}
                            // setBillAccountAttribute={setBillAccountAttribute}
                            // setBillAccountAttributes={setBillAccountAttributes}
                            // removeBillAccountAttribute={removeBillAccountAttribute}
                            // resetAttribute={resetAttribute}
                            handleSubmit={handleNewBillAccountSubmit}
                            handleValidateField={handleValidateField}
                        // error={error}
                        />
                    </> :
                    <>Setting data</>
            }

        </Dialog>
    )
}

export default AddAccountModal