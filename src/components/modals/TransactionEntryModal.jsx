import { Button, Dialog, Grid, InputLabel, MenuItem, OutlinedInput, Paper, Select, TextField, Typography } from '@mui/material'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import useForm from '../../hooks/useForm';
import { GridRowSection, StyledTextField } from '../../styles/login/styles'
import { setTransactionAttribute, setTransactionError, removeError, resetError, resetAttribute } from "../../redux/transactionSlice"
import useFormSubmit from "../../hooks/useSubmit"
import { useFormFieldsHandle, useFormValidateAndSubmit } from '../../hooks/useFormValidateSubmit';
import { transactionFormValidation } from '../../formValidations/transactionFormValidation';
import { BASE_URL } from '../../constants';

const TransactionEntryModal = ({ open, handleClose }) => {

  const dispatch = useDispatch()
  const transaction = useSelector(state => state.transaction.attributes)
  const error = useSelector(state => state.transaction.error)

  const { handleTextChange, handleListChange, handleSelectChange, handleSetAllFields, resetAllAttributes } = useFormFieldsHandle({
    setFieldAttribute: setTransactionAttribute,
    resetFieldAttributes: resetAttribute
  })

  const { handleValidateAndSubmit, handleValidateField, isLoading } = useFormValidateAndSubmit({
    url: `${BASE_URL}/transaction/add`,
    method: "post",
    formValidation: transactionFormValidation,
    errorState: error,
    setErrorState: setTransactionError
  })

  const userDataFethed = {
    "banks": ["SBI", "HDFC", "UNION"]
  }



  // const userDataFethed = {
  //   "username": "vedanthi@gmail.com",
  //   "password": "",
  //   // "role": "Admin",
  //   "country": "India",
  //   "address": "No. 1/1, Sumukha Residency SF 203, Arehalli, Uttrahalli",
  //   "userCreatedOn": "10-10-2022 12:00:00",
  //   "name": {
  //     "firstName": "Vedanthi",
  //     "lastName": "Karthik N"
  //   },
  //   "pin": "560061",
  //   "phoneNumber": "+91 9008784108",
  //   "dob": "04-04-1998",
  //   "userId": "sfew432efgset4bf",
  //   "ip": "223.434.4243",
  //   "banks": ["SBI", "HDFC", "UNION"]
  // }

  // const [dateNow, setDateNow] = React.useState(new Date().toISOString().split('T')[0])
  // const [transactionType, setTransactionType] = React.useState('')
  // const [transactionDirection, setTransactionDirection] = React.useState('In')
  // const [bank, setBank] = React.useState('')

  const accountId = localStorage.getItem("activeAccount")
  console.log("storage accountId: ", accountId)

  // console.log(transaction.transactionTo)

  // const addRequestBody = {
  //   "billAccountId": accountId,
  //   "transactionList": [transaction]
  // }

  // const { handleChange, handleValidate, handleSelectChange, submitValidation, setSubmitValidation } = useForm(transaction, error, setTransactionAttribute, () => { }, setTransactionError, removeError)
  // const { isPending, response, setIsPending, handleSubmit, handleSubmitOnClick, errorResponse } = useFormSubmit("http://localhost:8080/transaction/add", addRequestBody)

  // console.log(submitValidation, "error on toggled: ", error)

  // const handleFormSubmit = (event) => {
  //   event.preventDefault()
  //   handleValidate()
  // }

  useEffect(() => {
    return () => {
      dispatch(resetAttribute())
    }
  }, [])


  const handleNewBillAccountSubmit = async (e) => {
    e.preventDefault()
    let requestBody = {
      "billAccountId": accountId,
      "transactionList": [transaction]
    }

    console.log("component request: ", requestBody)

    const response = await handleValidateAndSubmit(requestBody)
    console.log("component rsponse: ", response)

    if (response.error === null) {
      handleClose()
    }
    else {
      console.log(response.status === 400)
      if (response.status === 400) {
        console.log("Setting Error: ", response.error?.details)
        dispatch(setTransactionError({ ...response.error?.details }))
      }
    }

  }

  // useEffect(() => {
  //   if (response !== null) {
  //     console.log("transaction submitted....")
  //   }
  // }, [response])



  // useEffect(() => {
  //   if (submitValidation) {
  //     console.log("submit validate error : ", error, "::", Object.keys(error).length)
  //     if (Object.keys(error).length === 0 && Object.keys(addRequestBody.transactionList).length !== 0) {
  //       handleSubmit()
  //       setSubmitValidation(false)
  //       // dispatch(resetAttribute())
  //       // console.log("closing model.....")
  //       // handleClose()
  //     }
  //   }
  // }, [submitValidation, error])

  // useEffect(() => {
  //   console.log("Response: ", response)
  //   if (response !== null) {
  //     dispatch(resetAttribute())
  //     console.log("closing model.....")
  //     handleClose()
  //   }
  //   console.log(!!errorResponse)
  // }, [response, errorResponse])



  return (
    <Dialog

      open={open}
      onClose={handleClose}
    >
      <Paper
        sx={{

          width: {
            // mobile: "90vw",
            tablet: "60vw"
          },
          height: {
            mobile: "90vh",
            tablet: "max-content"
          },

          padding: "5%",
          // backgroundColor: "red"
        }}>
        {/* <GridRowSection> */}
        <Grid container textAlign="-wenkit-center" justifyContent="center">
          <Typography variant='h6' align='center' color="secondary"
            sx={{
              marginBottom: 2,
              borderBottom: "2px solid red",
              width: "50%",
            }}
          >ADD TRANSACTION</Typography>
        </Grid>
        {/* </GridRowSection> */}

        <form onSubmit={handleNewBillAccountSubmit} >
          <Grid container mobile={12} paddingTop={3}
          >
            <GridRowSection
              sx={{
                justifyContent: {
                  mobile: "start"
                }
              }}
            >
              <StyledTextField
                variant='filled'
                fieldwidth={5}
                fieldlabel="Name"
                id="transactionFrom"
                name="transactionFrom"
                error={!!error.transactionFrom}
                helperText={!!error.transactionFrom && error.accountName}
                // type="number"
                disabled
                // onLoad={handleunhandled}
                // defaultValue={userDataFethed.name.firstName}
                value={transaction.transactionFrom}
              // onChange={handleChange}
              />
              {/* <TextField variant='standard' label='Name' fullWidth disabled id='transactionBy' value={userDataFethed.name.firstName} /> */}
            </GridRowSection>
            {/* <Grid item mobile={12}>
              <TextField variant='standard' fullWidth disabled id='transactionBy' value={userDataFethed.name.firstName} />
            </Grid> */}
            <GridRowSection >
              <StyledTextField
                variant='standard'
                fieldwidth={5}
                fieldlabel="Amount"
                id="transactionAmount"
                type="number"
                name="transactionAmount"
                error={!!error.transactionAmount}
                helperText={!!error.transactionAmount && error.transactionAmount}
                onChange={handleTextChange}
              // disabled={editDisabled}
              // defaultValue={userDataFethed.address}
              />

              <StyledTextField
                variant='standard'
                fieldwidth={5}
                fieldlabel="To"
                id="transactionTo"
                name="transactionTo"
                onChange={handleTextChange}
                error={!!error.transactionTo}
                helperText={!!error.transactionTo && error.transactionTo}
              // disabled={editDisabled}
              // defaultValue={userDataFethed.address}
              />
              {/* <TextField variant='standard' focused label="Amount" id='amount' type='number' />
              <TextField variant='standard' focused label="To" id='transactionTo' /> */}
            </GridRowSection>
            <GridRowSection>
              <StyledTextField
                variant='standard'
                fieldwidth={5}
                fieldlabel="Date of Transaction"
                id="transactionDate"
                name="transactionDate"
                type="date"
                onChange={handleTextChange}
                error={!!error.transactionDate}
                helperText={!!error.transactionDate && error.transactionDate}
                // disabled={editDisabled}
                defaultValue={new Date().toJSON().slice(0, 10)}
              />
              {/* <TextField variant='standard' focused label="Date of Transaction" id='transactionBy' type='date'
              /> */}
              {/* <InputLabel id="demo-multiple-name-label">Name</InputLabel>
              <Select>
                <MenuItem>Cash</MenuItem>
                <MenuItem>Credit/Debit Card</MenuItem>
                <MenuItem>Bank Transfer</MenuItem>
              </Select> */}
              <Grid item mobile={5}>
                {/* <InputLabel shrink id="transactionBank">Bank</InputLabel> */}
                <StyledTextField
                  select
                  variant='standard'
                  fullWidth value={transaction.transactionBank}
                  fieldlabel="Bank"
                  onChange={handleSelectChange}
                  name='transactionBank'
                  error={!!error.transactionBank}
                  helperText={!!error.transactionBank && error.transactionBank}

                >
                  {userDataFethed.banks.map((value, key) => (
                    <MenuItem key={key} value={value}>{value}</MenuItem>
                  ))}
                </StyledTextField>
              </Grid>

            </GridRowSection>
            <GridRowSection >
              <Grid item mobile={5}  >
                {/* <InputLabel shrink id="transactionType">Type</InputLabel> */}
                {/* <InputLabel id="demo-select-small">Age</InputLabel> */}
                <StyledTextField
                  select
                  fullWidth
                  // onChange={handleTypeChange}
                  onChange={handleSelectChange}
                  value={transaction.transactionType}
                  fieldlabel="Type"
                  variant='standard'
                  name="transactionType"
                  error={!!error.transactionType}
                  helperText={!!error.transactionType && error.transactionType}

                // placeholder='Select Type'
                >
                  <MenuItem value="Cash">Cash</MenuItem>
                  <MenuItem value="Credit/Debit Card">Credit/Debit Card</MenuItem>
                  <MenuItem value="Bank Transfer">Bank Transfer</MenuItem>
                </StyledTextField>
              </Grid>

              <Grid item mobile={5} >
                {/* <InputLabel shrink id="transDirection">Debit/Credit</InputLabel> */}
                {/* <InputLabel id="demo-select-small">Age</InputLabel> */}
                <StyledTextField
                  select
                  fullWidth
                  onChange={handleSelectChange}
                  value={transaction.transDirection}
                  fieldlabel="Debit/Credit"
                  variant='standard'
                  name='transDirection'
                  error={!!error.transDirection}
                  helperText={!!error.transDirection && error.transDirection}
                >
                  <MenuItem value="In">Credit</MenuItem>
                  <MenuItem value="Out">Debit</MenuItem>
                </StyledTextField>
              </Grid>
            </GridRowSection>
            <GridRowSection>
              <StyledTextField
                variant='outlined'
                fullWidth
                multiline
                rows={2}
                fieldwidth={12}
                fieldlabel="Description"
                name="description"
                id="description"
                onChange={handleTextChange}
                error={!!error.description}
                helperText={!!error.description && error.description}
              // disabled={editDisabled}
              // defaultValue={userDataFethed.address}
              />
            </GridRowSection>
            <GridRowSection sx={{ justifyContent: "right" }} gap={2}>
              <Button
                variant='outlined'
              >Reset</Button>
              <Button
                type='submit'
                variant='contained'
              >Submit</Button>
            </GridRowSection>
          </Grid>
        </form>
      </Paper>

    </Dialog >
  )
}

export default TransactionEntryModal