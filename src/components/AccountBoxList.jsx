import { Add } from '@mui/icons-material'
import { Box, Grid } from '@mui/material'
import { useTheme } from '@mui/styles'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import AddAccountModal from './modals/AddAccountModal'

const AccountBoxList = ({
    account,
    hardLoad
    // customOnClick
}) => {
    const theme = useTheme()
    const navigate = useNavigate()

    const [accountModalState, setAccountModalState] = useState(false)

    const handleClick = () => {
        setAccountModalState(true)
    }

    const handleAddAccountModel = () => {
        setAccountModalState(false)
        hardLoad()
    }



    console.log("Accountlist", !!account)
    return (
        // <>
        <Grid item mobile={6} tablet={4} laptop={3}
            sx={{
                textAlign: {
                    mobile: "-webkit-center",
                    tablet: "start"
                }
            }}
        >
            <Box
                sx={{
                    margin: {
                        mobile: "2%",
                        tablet: "5%"
                    },
                    height: {
                        mobile: "150px",
                        tablet: "160px",
                        laptop: "200px"
                    },
                    maxWidth: {
                        mobile: "200px",
                        tablet: "250px"
                    },
                    borderRadius: "20px",
                    display: "flex",
                    color: "white",
                    justifyContent: "center",
                    alignItems: "center",
                    cursor: "pointer",
                    backgroundColor: account === undefined ? "none" : theme.palette.primary.main,
                    // backgroundImage: `linear-gradient(to bottom right, rgb(${255 / 1},0,0), yellow)`
                    backgroundImage: `linear-gradient(to bottom right, ${theme.palette.primary.main}, rgba(${Math.floor(Math.random() * 256)},${Math.floor(Math.random() * 150)},${Math.floor(Math.random() * 100)},0.5))`,

                }}
                onClick={handleClick}
            // onClick={customOnClick}
            // onClick={account === undefined ? () => navigate("account/create") : () => { }}
            // onClick={account === undefined ? () => setAddAccountModalState(true) : () => {
            //     setAddAccountModalState(true)
            //     setBillAccountDetail(account)
            // }}

            >
                <Box sx={{
                    padding: account === undefined ? 'none' : "5% 15%",
                    backgroundColor: account === undefined ? 'none' : 'rgb(0, 0, 0)',
                    backgroundColor: account === undefined ? 'none' : 'rgba(0, 0, 0, 0.2)',
                    // border: '2px solid blue',
                    borderRadius: account === undefined ? 'none' : 2
                }}>
                    {account === undefined ? <Add /> : account.accountName}
                </Box>
            </Box>
            {accountModalState && <AddAccountModal open={accountModalState} handleClose={handleAddAccountModel} billAccountDetail={account} />}

        </Grid>
        // </>
    )
}

export default AccountBoxList