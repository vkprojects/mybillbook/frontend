import { useLocation, Navigate, Outlet } from "react-router-dom";
import useAuth from "../hooks/useAuth";
import { useEffect } from "react";

export const RequiredAuth = () => {

    const { auth } = useAuth()
    useEffect(() => {
        console.log("Required Auth loaded")
        console.log("Required Auth status: ", auth)
    }, [])


    return (
        <>
            {console.log("returning page to login:", !auth)}
            {
                auth
                    ? <Outlet />            //AllChildren
                    : <Navigate to="/login" replace />
            }
        </>

    )
}

export default RequiredAuth