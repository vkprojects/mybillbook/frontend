import { Person } from '@mui/icons-material';
import { Box, Button, Grid, Typography } from '@mui/material';
import { useTheme } from '@mui/styles';
import React from 'react';
import { GridRowSection, StyledTextField } from '../styles/login/styles';
import "../styles/profile.css";
// import useFetch from '../hooks/useFetchData';

const Profile = () => {

    const [editDisabled, setEditDisabled] = React.useState(true)
    // const userFormDb = useFetch("http://localhost:8080/signin")

    const userDataFethed = {
        "email": "vedanthikarthik@gmail.com",
        "password": "",
        // "role": "Admin",
        "country": "India",
        "address": "No. 1/1, Sumukha Residency SF 203, Arehalli, Uttrahalli",
        "userCreatedOn": "10-10-2022 12:00:00",
        "name": {
            "firstName": "Vedanthi",
            "lastName": "Karthik N"
        },
        "pin": "560061",
        "phoneNumber": "+91 9008784108",
        "dob": "04-04-1998",
        "userId": "sfew432efgset4bf",
        "ip": "223.434.4243"
    }

    const theme = useTheme()

    return (

        //main container
        <Grid container sx={{
            boxShadow: "hsl(280deg 70% 35% / 75%) 0px 0px 5px 0px",
            // height: "100%",
            borderRadius: "10px",
            overflow: "hidden",
            marginBottom: "5%"
        }}>
            {/* top header palette */}
            <Grid
                container
                sx={{
                    minHeight: "10%",
                    height: "max-content",
                    color: "white",
                    backgroundColor: theme.palette.primary.main,
                }}>
                <Grid item mobile={12} sx={{ padding: "2%", display: "flex", alignItems: "center" }}>
                    <Typography variant='h4'>Profile</Typography>
                </Grid>
            </Grid>

            {/* data section */}
            <Grid container mobile={12} sx={{ backgeoundColor: "blue", padding: "2% 5%" }}>
                {/* row1 */}
                <GridRowSection
                    sx={{
                        justifyContent: {
                            mobile: "start"
                        }
                    }}>
                    <Grid container mobile={6}>
                        <Grid item >
                            <Box
                                sx={{
                                    width: "60px",
                                    height: "60px",
                                    borderRadius: "50%",
                                    border: "1px dashed red"
                                    // backgroundColor: "yellow"
                                }}
                                variant="div">
                                <Person />
                            </Box>
                        </Grid>
                        <Grid item sx={{ marginLeft: "30px" }}>
                            <Grid item><Typography variant='h6' color="secondary">{userDataFethed.name.firstName} {userDataFethed.name.lastName}</Typography></Grid>
                            <Grid item><Typography variant='body2'>{userDataFethed.email}</Typography> </Grid>
                        </Grid>
                    </Grid>

                    <Grid item mobile={6} sx={{ textAlign: "right" }}>
                        <Button variant='contained'
                            onClick={e => { setEditDisabled(false) }}
                            sx={{ marginRight: "20px" }}>
                            Edit
                        </Button>
                    </Grid>
                </GridRowSection>
                <Typography sx={{ fontWeight: "bold", margin: "20px 0", borderBottom: "2px solid red" }}>Personal Information</Typography>

                <GridRowSection gap={2}
                    sx={{
                        justifyContent: {
                            mobile: "start"
                        }
                    }}
                >
                    {/* <Grid container mobile={12}> */}
                    <StyledTextField
                        variant={editDisabled ? "standard" : "outlined"}
                        fieldwidth={3}
                        fieldlabel="First Name"
                        id="fname"
                        disabled={editDisabled}
                        defaultValue={userDataFethed.name.firstName}
                        InputProps={{
                            disableUnderline: true
                        }}
                    />
                    <StyledTextField
                        variant={editDisabled ? "standard" : "outlined"}
                        fieldwidth={3}
                        fieldlabel="last Name"
                        id="lname"
                        disabled={editDisabled}
                        defaultValue={userDataFethed.name.lastName}
                        InputProps={{
                            disableUnderline: true
                        }}
                    />
                    <StyledTextField
                        variant={editDisabled ? "standard" : "outlined"}
                        fieldwidth={3}
                        fieldlabel="Date of Birth"
                        id="dob"
                        disabled={editDisabled}
                        defaultValue={userDataFethed.dob}
                        InputProps={{
                            disableUnderline: true
                        }}
                    />
                    {/* </Grid> */}
                    {/* <Grid container mobile={12}>

                    </Grid> */}
                </GridRowSection>

                <GridRowSection gap={1}
                    sx={{
                        justifyContent: {
                            mobile: "start"
                        }
                    }}>
                    <StyledTextField
                        variant={editDisabled ? "standard" : "outlined"}
                        fieldwidth={3}
                        fieldlabel="Country"
                        id="country"
                        disabled={editDisabled}
                        defaultValue={userDataFethed.country}
                        InputProps={{
                            disableUnderline: true
                        }}
                    />

                    <StyledTextField
                        variant={editDisabled ? "standard" : "outlined"}
                        multiline
                        fullWidth
                        fieldwidth={5}
                        fieldlabel="Address"
                        id="address"
                        disabled={editDisabled}
                        defaultValue={userDataFethed.address}
                        InputProps={{
                            disableUnderline: true
                        }}
                    />
                    <StyledTextField
                        variant={editDisabled ? "standard" : "outlined"}
                        multiline
                        fieldwidth={3}
                        fieldlabel="Pin Code"
                        id="pin"
                        disabled={editDisabled}
                        defaultValue={userDataFethed.pin}
                        InputProps={{
                            disableUnderline: true
                        }}
                    />
                </GridRowSection>

                <GridRowSection
                    // mobile={4}
                    sx={{
                        justifyContent: {
                            mobile: "start"
                        },
                    }}>
                    <StyledTextField
                        variant={editDisabled ? "standard" : "outlined"}
                        fieldwidth={3}
                        // type="number"
                        fieldlabel="Phone"
                        id="phone"
                        disabled={editDisabled}
                        defaultValue={userDataFethed.phoneNumber}
                        InputProps={{
                            disableUnderline: true
                        }}
                    />
                </GridRowSection>
                <GridRowSection gap={2} justifyContent="right">
                    <Button
                        variant='outlined'
                        onClick={e => { setEditDisabled(true) }}
                        sx={{
                            display: editDisabled ? "none" : "inline-block"
                        }}
                    >
                        Cancel
                    </Button>
                    <Button
                        variant='contained'
                        type='submit'
                        sx={{
                            display: editDisabled ? "none" : "inline-block"
                        }}
                    >
                        Submit
                    </Button>
                </GridRowSection>


                {/* </Grid> */}
                {/* <Grid item mobile={6} tablet={12} sx={{ padding: "5px" }}>
                    <Typography variant='body1'>{userDataFethed.name.firstName} {userDataFethed.name.lastName}</Typography>
                </Grid>
                <Grid item mobile={6} tablet={12} sx={{ padding: "5px" }} >
                    <Typography variant='body2'>{userDataFethed.username}</Typography>
                </Grid> */}
            </Grid>

        </Grid>
        // </div>
    )
}


export default Profile