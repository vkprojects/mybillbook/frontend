import { Box, Grid } from '@mui/material'
import React, { useEffect, useState } from 'react'
import useFetchData from '../hooks/useFetchData'
import { useCookies } from 'react-cookie';
import { Add } from '@mui/icons-material';
import { useTheme } from '@mui/styles';
import AddAccountModal from "./modals/AddAccountModal"
import { redirect, useNavigate } from 'react-router-dom';
import AccountBoxList from './AccountBoxList';
import { BASE_URL } from '../constants';

const Accounts = () => {
    const [cookies] = useCookies(['user']);
    const user = cookies.user
    const theme = useTheme()
    // const [addAccountModalState, setAddAccountModalState] = useState(false)
    // const [billAccountDetail, setBillAccountDetail] = useState(null)
    const [loadComponent, setLoadComponent] = useState(false)
    const { response: accountListResponse, isLoading: accountListLoading, error: accountListError } = useFetchData(`${BASE_URL}/account/search?accountUserList=${user}`, loadComponent)


    useEffect(() => {
        console.log("Accounts componenet loaded")
    }, [])

    const hardLoad = () => {
        console.log("hard load")
        setLoadComponent(prev => !prev)
    }

    // const handleAddAccountModel = () => {
    //     setAddAccountModalState(false)
    // }
    // const navigate = useNavigate()

    return (
        accountListLoading ? <h5>Loading...</h5> :
            <Grid container
                sx={{
                    justifyContent: {
                        mobile: "space-between",
                        tablet: "start"
                    },
                }}
            >
                {!!accountListResponse?.details &&
                    accountListResponse?.details.map((account, key) =>
                        <AccountBoxList
                            key={key}
                            account={account}
                            // setAddAccountModalState={setAddAccountModalState}
                            // setBillAccountDetail={setBillAccountDetail}
                            hardLoad={hardLoad}
                        // customOnClick={() => {
                        //     setAddAccountModalState(true)
                        //     setBillAccountDetail(account)
                        // }}
                        />
                    )
                }
                <AccountBoxList
                    key="new account"
                    // setAddAccountModalState={setAddAccountModalState}
                    hardLoad={hardLoad}

                // customOnClick={() => {
                //     setBillAccountDetail(null)
                //     setAddAccountModalState(true)
                // }}


                />
                {/* {addAccountModalState && <AddAccountModal open={addAccountModalState} handleClose={handleAddAccountModel} billAccountDetail={billAccountDetail} />} */}

            </Grid>
    )


}



export default Accounts