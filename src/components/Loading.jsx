import { Dialog, Grid, Paper } from '@mui/material'
import React from 'react'
import HourglassTopOutlinedIcon from '@mui/icons-material/HourglassTopOutlined';

const Loading = ({ open }) => {
    return (
        <Dialog
            open={open}
            disableEscapeKeyDown={true}
            // fullScreen={true}
            // aria-labelledby="alert-dialog-title"
            // aria-describedby="alert-dialog-description"
            sx={{
                // height: "100%",
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                // opacity: 0
                // marginInline: '50%'
            }}
        >
            {/* <Grid container sx={{ height: "100%", justifyContent: "center", alignItems: "center" }}> */}
            {/* <Grid item sx={{ textAlign: 'center' }}> */}
            <HourglassTopOutlinedIcon />
            {/* </Grid> */}
            {/* </Grid> */}

        </Dialog>
    )
}

export default Loading