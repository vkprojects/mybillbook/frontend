import React, { useEffect, useState } from 'react'
import AccountTransactionView from './AccountTransactionView';
import useFetchData from '../hooks/useFetchData';
import { useCookies } from 'react-cookie';
import { Button, Grid, Typography } from '@mui/material';
import { Add } from '@mui/icons-material';
import AddAccountModal from './modals/AddAccountModal';
import { BASE_URL } from '../constants';
import Loading from './Loading';


const AccountHomePageView = () => {

    const [cookies] = useCookies(['user']);
    const [loadComponent, setLoadComponent] = useState(false)
    console.log("Account homepage View", loadComponent)
    const user = cookies.user
    console.log("userCookie: ", user)
    const { response: accountListResponse, isLoading: accountListLoading, error: accountListError } = useFetchData(`${BASE_URL}/account/search?accountUserList=${user}`, loadComponent)

    console.log("Account Response", accountListResponse)
    console.log("Account Error", accountListError)

    // useEffect(() => {
    //     console.log("loading", accountListLoading)
    // }, [loadComponent])


    // console.log("AccountList: ", accountList, accountListLoading, accountListLoading ? "A" : (!!accountList && accountList.length === 0))
    // console.log("Accounts List Error: ", accountListError?.details.includes("No result found"))


    if (accountListLoading) {
        console.log("Loading: ", accountListLoading)
        // return <h4>Loading...</h4>
        return <Loading open={accountListLoading} />

    }
    else if (!!accountListError && accountListError?.details.indexOf("No result found") != -1) {
        console.log("No Accounts")
        return <CreateNewAccount setLoadComponent={setLoadComponent} />
    }
    else {
        // const { details: accountList } = !!accountListError ? [] : accountListResponse;
        const accountList = accountListError ? [] : accountListResponse?.details

        return (!!accountList && accountList.length === 0) ?
            <h4>Oops!!! Something went wrong</h4> :
            <AccountTransactionView accountList={accountList} />
    }

}

const CreateNewAccount = ({ setLoadComponent }) => {
    const [accountCreateModalState, setAcccountCreateModalState] = useState(false)
    const handleCreateModalState = (event) => {
        setAcccountCreateModalState(false)
        setLoadComponent(prev => !prev)
    }

    return (
        <Grid container sx={{ gap: 2, height: "100%", justifyContent: "center", alignItems: "center" }}>
            <Grid item sx={{ textAlign: 'center' }}>
                <Typography variant='body2' sx={{ margin: 2 }}>Log transactions by creating your first account here</Typography>
                <Button
                    variant='contained'
                    onClick={() => setAcccountCreateModalState(true)} >
                    Create
                </Button>
                <AddAccountModal open={accountCreateModalState} handleClose={handleCreateModalState} />
            </Grid>
        </Grid>
    )

}
// (accountList !== null || accountList.length !== 0) ? <AccountTransactionView accountList={accountList} /> : <h3>No Account Found create new Transaction</h3>







// return (
//     {
//         accountList !== null || accountList.length === 0 ?
//         <AccountTransactionView/>:<h3>No Account Found create new Transaction</h3>
//     }
// )



export default AccountHomePageView