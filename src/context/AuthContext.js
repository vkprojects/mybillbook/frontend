import { createContext, useEffect, useState } from "react";
import useAuthData from "../hooks/useAuthData";
import { useCookies } from "react-cookie";

const AuthContext = createContext({})

export const AuthProvider = ({ children }) => {
    // const [auth, setAuth] = useState(!!localStorage.getItem("sessionLoginStatus"))
    const { getAuthData, removeAuthAuthdata } = useAuthData()

    const [{ user: activeUser }] = useCookies(['user']);
    const [auth, setAuth] = useState(() => {

        const sessionData = getAuthData()
        if (sessionData.userToken == null || activeUser == null) {
            return false
        }
        else {
            return true
        }
    })



    useEffect(() => {
        if (!auth) {
            console.log("Auth, user", auth, activeUser)
            removeAuthAuthdata()
        }
        // const sessionData = getAuthData()
        // console.log("Auth Provider loading", typeof localStorage.getItem("sessionLoginStatus"), localStorage.getItem("sessionLoginStatus"), typeof sessionData.loginStatus)
        // console.log("login status", !!localStorage.getItem("sessionLoginStatus"), sessionData.loginStatus == true, sessionData.userToken !== null)
        // if (!!localStorage.getItem("sessionLoginStatus") && sessionData.userToken !== null) {
        //     console.log("local auth: ", !!localStorage.getItem("sessionLoginStatus") && sessionData.userToken !== null)
        //     setAuth(true)
        // }
        // else {
        //     console.log("local auth: ", !!localStorage.getItem("sessionLoginStatus") && sessionData.userToken !== null)
        //     removeAuthAuthdata()
        // }
    }, [auth])

    return (
        <AuthContext.Provider value={{ auth, setAuth }}>
            {children}
        </AuthContext.Provider>
    )
}

export default AuthContext