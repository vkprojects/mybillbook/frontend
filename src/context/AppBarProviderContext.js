import React, { createContext, useEffect, useState } from 'react'
import { useNavigate, useRoutes } from 'react-router-dom'

const AppbarContext = createContext()

export const AppBarProviderContext = ({ children }) => {
  const [currentIndex, setCurrentIndex] = useState(0)
  const navigate = useNavigate()
  // const route = useRoutes();
  // console.log(route.name);

  useEffect(() => {
    // navigate("/")
  }, [currentIndex])


  return (
    <AppbarContext.Provider value={{ currentIndex, setCurrentIndex }}>
      {children}
    </AppbarContext.Provider>
  )
}

export default AppbarContext